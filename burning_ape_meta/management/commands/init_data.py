from django.core.management.base import BaseCommand
from burning_ape_meta.models.Address import Address
from burning_ape_meta.models.ContactMechanism import ContactMechanism
from burning_ape_meta.models.Country import Country
from burning_ape_meta.models.Field import Field
from burning_ape_meta.models.Order import Order
from burning_ape_meta.models.OrderItem import OrderItem
from burning_ape_meta.models.Person import Person
from burning_ape_meta.models.Player import Player
from burning_ape_meta.models.PlayerClub import PlayerClub
from burning_ape_meta.models.Product import Product
from burning_ape_meta.models.Club import Club
from burning_ape_meta.models.Tournament import Tournament
from burning_ape_meta.models.TournamentField import TournamentField
from burning_ape_meta.models.TournamentProduct import TournamentProduct
from burning_ape_meta.models.TournamentTeam import TournamentTeam
from burning_ape_meta.models.TournamentTeamPlayer import TournamentTeamPlayer
from burning_ape_meta.models.ZipCode import ZipCode
from django.db import transaction
from faker import Faker

import json
import random


countries_file_name = 'countries.txt'
zip_codes_file_name = 'plz_verzeichnis_v2.json'

class Command(BaseCommand):
    help = 'Initialize the country and zip-code tables'
    fake = Faker()


    @transaction.atomic
    def handle(self, *args, **options):
        self.init_zips() if Country.objects.count() == 0 else None
        self.init_address()
        self.init_tournament()
        self.init_player()
        self.init_club()
        self.init_player_club()

        self.init_field()
        self.init_tournament_field()

        self.init_product()
        self.init_tournament_product()
        self.init_contact_mechanism()
        self.init_tournament_team()
        self.init_tournament_team_player()


    def init_zips(self):
        i = 1;
        countries = []
        with open(countries_file_name, 'r') as file:
            while (line := file.readline().rstrip()):
                countries.append(Country(country=line))

        Country.objects.bulk_create(countries)

        with open(zip_codes_file_name, 'r') as file:
            zip_code_data = json.load(file)

        country = Country.objects.get(country='Switzerland')
        zip_codes = set()
        for entry in zip_code_data:
            zip_codes.add(":".join([
                str(entry['fields']['postleitzahl']),
                entry['fields']['ortbez27']
            ]))

        unique_members = list(set(zip_codes))
        for entry in unique_members:
            ZipCode(
                id = i,
                country=country,
                zip_code=entry.split(":")[0],
                locality=entry.split(":")[1]
            ).save()
            i += 1

    def init_address(self):
        zip_count = ZipCode.objects.count()
        for x in range(100):
            Address(
                id = x + 1,
                line1 = self.fake.address(),
                zip_code_id = random.randint(1, zip_count)
            ).save()


    def init_tournament(self):
        self.create_tournament(1, 'Banana Cup')
        self.create_tournament(2, 'Burning Ape 3.0')
        for x in range(5):
            self.create_tournament(x + 3)

    def create_tournament(self, n, name = None):
            name = self.fake.company() if name == None else name
            Tournament(
                id = n,
                name=name,
                location_id=random.randint(1, 100),
                date=self.fake.date()
            ).save()

    def init_player(self):
        for x in range(200):
            Player(
                id = x + 1,
                player_number=x,
                first_name=self.fake.first_name(),
                last_name=self.fake.last_name(),
            ).save()


    def init_club(self):
        Club(id = 1, name='Scorillaz').save()
        for x in range(20):
            Club(id = x + 2, name = self.fake.name()).save()


    def init_player_club(self):
        for player_id in range(1, 200):
            PlayerClub(
                club_id = random.randint(1, 20),
                player_id = player_id
            ).save()

    def init_tournament_team(self):
        TournamentTeam(
            id = 1,
            team_name='Scorillaz',
            registration_date = self.fake.date(),
            tournament_id = 1,
            contact_id = 1,
        ).save()

        for x in range(5):
            TournamentTeam(
                id = x + 2,
                team_name = self.fake.name(),
                registration_date = self.fake.date(),
                tournament_id = x + 2,
                contact_id = random.randint(1, 200),
            ).save()


    def init_tournament_team_player(self):
        for player_id in range(200):
            TournamentTeamPlayer(
                tournament_team_id = random.randint(1, 6),
                player_id = player_id + 1,
            ).save()

    def init_field(self):
        for x in range(10):
            Field(
                id = x + 1,
                name = self.fake.name(),
                length = random.randint(80, 100),
                width = random.randint(40, 60),
                location_id = random.randint(1, 100),
            ).save()

    def init_tournament_field(self):
        for tournament_id in range(1, 7):
            TournamentField(
                tournament_id = tournament_id,
                field_id = random.randint(1, 9)
            ).save()

    def init_product(self):
        for x in range(20):
            Product(
                id = x + 1,
                product_name = self.fake.name(),
                price = random.randint(2, 14)
            ).save()

    def init_tournament_product(self):
        for tournament_id in range(1, 7):
            TournamentProduct(
                tournament_id = tournament_id,
                product_id = random.randint(1, 20)
            ).save()


    def init_contact_mechanism(self):
        for person in Person.objects.all():
            cm = ContactMechanism(contact_type = ContactMechanism.ContactType.Mail, email = self.fake.email())
            cm.save()
            person.contact_mechanism.add(cm)

