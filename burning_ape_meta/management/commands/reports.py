from burning_ape.settings import REPORT_ROOT
from burning_ape_meta.models.Order import Order
from burning_ape_meta.models.OrderItem import OrderItem
from burning_ape_meta.models.Tournament import Tournament
from burning_ape_meta.managers.OrderManager import OrderManager
from django.core.management.base import BaseCommand
from django.db.models import Count
from qrbill import QRBill
from reportlab.graphics import renderPDF
from svglib.svglib import svg2rlg

import os
import pylatex
import tempfile


class Letter(pylatex.base_classes.Environment):
    """A class to wrap LaTeX's letter environment."""

    escape = False
    content_separator = "\n"


class Command(BaseCommand):
    help = 'Generate reports for a tournament'

    komavars = {
        'fromaddress': '',
        'fromname':  r'',
        'fromphone': r'',
        'fromemail': r'',
        'date': r'\today',
        'place': r'',
        'signature': r'',
    }

    qrbill = {
        'account': '',
        'creditor': {
            'name':    '',
            'pcode':   '',
            'city':    '',
            'country': '',
        },
        'amount': 0,
        'currency': '',
        'additional_information': '',
    }
    letter_content = r"""
    Thank you for joining us at the Banana Cup 3.0! We hope you enjoyed
    yourselves. Please feel free to drop us a note if you feel like anything was
    missing or could be improved!

    Attached you will find the invoice for your team.

    \closing{Kind Regards}

    \ps

    \textbf{PS:}
    We're looking forward to see you at the burning ape!

    \vspace*{\fill}
    """

    def add_arguments(self, parser):
        # Require tournament name
        parser.add_argument('tournament_name', nargs=1, type=str)

    def handle(self, *args, **options):
        tournament = Tournament.objects.filter(
            name=options['tournament_name'][0]
        ).first()

        owner = tournament.owner
        # TODO: add address and phone to person
        self.komavars['fromaddress'] = ''
        self.komavars['fromname'] = owner.full_name
        self.komavars['fromphone'] = r''
        self.komavars['fromemail'] = owner.email
        self.komavars['date'] = r'\today'
        self.komavars['place'] = ''
        self.komavars['signature'] = owner.full_name

        # TODO: add billing address to person
        teams = tournament.teams.all()

        for team in teams:
            players = team.tournament_team_players.all().values('player')
            total_team_price = OrderManager().get_total_price_for_persons(players)

            # skip teams without fees.
            if (total_team_price == 0):
                continue

            # create qr_bill
            docname = "{tournament}_{team_name}".format(
                tournament=str.lower(options['tournament_name'][0]).replace(' ', '_'),
                team_name=''.join(e for e in team.team_name if e.isalnum())
            )
            bill_filename = "{name}_qr.pdf".format(name=docname)
            self.qrbill['amount'] = total_team_price
            self.qrbill['additional_information'] = " - ".join([
                'BananaCup', team.team_name
            ])
            self.create_qr_bill_as_pdf(
                os.path.join(REPORT_ROOT, bill_filename)
            )

            # create latex document
            doc = pylatex.Document(documentclass='scrlttr2',
                                   document_options='enlargefirstpage')
            doc.preamble.append(pylatex.utils.NoEscape((
                r'\usepackage{babel}'
                r'\usepackage{graphicx}'
                r'\usepackage{tikz}'
                r'\usepackage{booktabs}'
                r'\usepackage[sfdefault]{FiraSans}'
                r'\usepackage[T1]{fontenc}'
                r'\renewcommand*\oldstylenums[1]{{\firaoldstyle #1}}'
            )))

            # set variables of letter
            for var in self.komavars:
                doc.append(pylatex.Command(
                    'setkomavar', arguments=[
                        pylatex.utils.NoEscape(var),
                        pylatex.utils.NoEscape(self.komavars[var])
                    ]))

            # write the letter
            with doc.create(Letter(arguments=[team.team_name])):
                doc.append(pylatex.Command(
                    'opening', arguments=[
                        " ".join(["Dear", team.team_name])
                    ]))
                doc.append(pylatex.NoEscape(self.letter_content))

                doc.append(pylatex.NoEscape("".join([
                    r'\begin{tikzpicture}[remember picture,overlay]', '\n'
                    r'\node[anchor=south east] at (current page.south east)', '\n',
                    r'{\includegraphics[width=\paperwidth]{',
                    bill_filename.replace(r'reports/', '').replace("svg", "png"),
                    r'}};', '\n'
                    r'\end{tikzpicture}'
                ])))

            doc.append(pylatex.NoEscape(r'\noindent\textbf{Overview per Player}'))
            doc.append(pylatex.NoEscape('\n'))

            with doc.create(pylatex.LongTable("l r r")) as data_table:
                data_table.add_hline()
                data_table.add_row([
                    "Name",
                    pylatex.MultiColumn(2, align='r', data="Total Amount")
                ])
                data_table.add_hline()
                data_table.end_table_header()
                data_table.add_hline()

                row = 0
                for team_player in team.tournament_team_players.order_by(
                        'player__first_name'
                ).all():
                    data_table.add_row([
                        team_player.player.full_name,
                        "{:10.2f}".format(
                            OrderManager().get_total_price_for_persons([
                                team_player.player
                            ])
                        ),
                        "CHF",
                    ], color=("lightgray!30" if row % 2 else "white"))
                    row += 1
                data_table.add_hline()

            doc.append(pylatex.NoEscape(r'\textbf{Bookings}'))
            doc.append(pylatex.NoEscape('\n'))
            doc.append(pylatex.NoEscape(r'{\scriptsize'))

            with doc.create(pylatex.LongTable("l l l r r")) as data_table:
                data_table.add_hline()
                data_table.add_row([
                    "Date",
                    "Name",
                    "Product",
                    pylatex.MultiColumn(2, align='r', data="Amount")
                ])
                data_table.add_hline()
                data_table.end_table_header()
                data_table.add_hline()

            row = 0
            for team_player in team.tournament_team_players.annotate(
                order_count=Count('player__orders')
            ).filter(
                order_count__gt=0
            ).order_by(
                'player__first_name'
            ).all():
                for order_item in OrderItem.objects.filter(
                        order__in=Order.objects.filter(
                            person=team_player.player)):
                    data_table.add_row([
                        order_item.order.order_datetime.strftime(
                            "%d.%b.%Y %H:%M"
                        ),
                        order_item.order.person.full_name,
                        "{count}x {product}".format(
                            count=order_item.quantity,
                            product=order_item.product.product_name
                        ),
                        "{:10.2f}".format(order_item.price *
                                          order_item.quantity),
                        "CHF",
                    ], color=("lightgray!30" if row % 2 else "white"))
                    row += 1
                data_table.add_hline()
            doc.append(pylatex.NoEscape('}'))

            docname = os.path.join(REPORT_ROOT, docname)
            doc.generate_pdf(docname, clean=False, clean_tex=False)

    def create_qr_bill_as_pdf(self, filename):
        bill = QRBill(**self.qrbill)
        with tempfile.TemporaryFile(encoding='utf-8', mode='r+') as temp:
            bill.as_svg(temp)
            temp.seek(0)
            drawing = svg2rlg(temp)
            renderPDF.drawToFile(drawing, filename)
