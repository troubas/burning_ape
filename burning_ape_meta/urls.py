
from django.urls import path, include
# from rest_framework.routers import DefaultRouter
from rest_framework_nested.routers import (
    DefaultRouter,
    NestedSimpleRouter,
)

from .views import (
    AddressViewSet,
    ClubViewSet,
    CountryViewSet,
    FieldViewSet,
    FieldTimeSlotViewSet,
    MatchViewSet,
    MatchTeamViewSet,
    OrderItemViewSet,
    PersonClubViewSet,
    PersonContactMechanismViewSet,
    PersonOrderViewSet,
    PersonViewSet,
    PhaseSeedViewSet,
    ProductViewSet,
    TournamentGroupViewSet,
    TournamentOrderViewSet,
    TournamentPhaseViewSet,
    TournamentProductViewSet,
    TournamentTeamPlayerViewSet,
    TournamentTeamViewSet,
    TournamentViewSet,
    UserViewSet,
    ZipCodeViewSet,
)

router = DefaultRouter()
router.register(r'adresses', AddressViewSet)
router.register(r'clubs', ClubViewSet)
router.register(r'countries', CountryViewSet)
router.register(r'fields', FieldViewSet)
router.register(r'tournaments', TournamentViewSet)
router.register(r'persons', PersonViewSet)
router.register(r'products', ProductViewSet)
router.register(r'users', UserViewSet)
router.register(r'zip_codes', ZipCodeViewSet)

field_router = NestedSimpleRouter(router, r'fields', lookup='field')
field_router.register(r'timeslots', FieldTimeSlotViewSet, basename='timeslots')

tr = NestedSimpleRouter(router, r'tournaments', lookup='tournament')
tr.register(r'groups', TournamentGroupViewSet, basename='tournamentgroup')
tr.register(r'matches', MatchViewSet)
tr.register(r'matchteams', MatchTeamViewSet)
tr.register(r'orders', TournamentOrderViewSet)
tr.register(r'phases', TournamentPhaseViewSet,
            basename='tournamentphase')
tr.register(r'phaseseeds', PhaseSeedViewSet)
tr.register(r'products', TournamentProductViewSet, basename='products')
tr.register(r'teams', TournamentTeamViewSet, basename='tournamentteam')
tr.register(r'tournamentteamplayers', TournamentTeamPlayerViewSet)

order_router = NestedSimpleRouter(tr, r'orders', lookup='order')
order_router.register(r'order_items', OrderItemViewSet, basename='orderitems')

user_router = NestedSimpleRouter(router, r'persons', lookup='person')
user_router.register(r'clubs', PersonClubViewSet, basename='personclubs')
user_router.register(r'orders', PersonOrderViewSet, basename='personorders')
user_router.register(r'tournamentteams',
                     TournamentTeamPlayerViewSet,
                     basename='tournamentteamplayers')

urlpatterns = [
    path(r'', include(router.urls)),
    path(r'', include(tr.urls)),
    path(r'', include(order_router.urls)),
    path(r'', include(user_router.urls)),
    path(r'', include(order_router.urls)),
]
