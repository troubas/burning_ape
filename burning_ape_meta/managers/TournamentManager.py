from burning_ape_meta.querysets.TournamentQuerySet import TournamentQuerySet

class TournamentManager:
    def get_queryset(self):
        return TournamentQuerySet()
