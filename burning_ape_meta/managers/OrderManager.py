from burning_ape_meta.models.Order import Order
from burning_ape_meta.models.Person import Person
from burning_ape_meta.querysets.OrderQuerySet import OrderQuerySet
from decimal import Decimal
from django.db import models
from django.db.models.query import QuerySet


class OrderManager(models.Manager):
    def get_queryset(self) -> OrderQuerySet:
        return OrderQuerySet(self.model, using=self._db)

    def get_persons_orders(self, persons: list[Person]) -> QuerySet[Order]:
        return self.get_queryset().get_persons_orders(persons)

    def get_total_price_for_persons(
        self, persons: list[Person]
    ) -> Decimal:
        return self.get_queryset().get_total_price_for_persons(persons)
