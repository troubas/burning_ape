from rest_framework.schemas.openapi import AutoSchema
from rest_framework.schemas.utils import is_list_view


class CustomQueryParamsSchema(AutoSchema):
    def __init__(self, query_params=[], operation_id=None, **kwargs):
        self.query_params = query_params
        self.operation_id = operation_id
        super().__init__(**kwargs)

    def get_operation(self, path, method):
        operation = super().get_operation(path, method)
        if not is_list_view(path, method, self.view):
            return operation

        for param in self.query_params:
            operation['parameters'].append(
                {
                    'name': param['name'],
                    'in': 'query',
                    'required': False,
                    'description': '',
                    'schema': {'type': param['type']}
                })

        return operation

    def get_operation_id_base(self, path, method, action):
        if self.operation_id:
            return self.operation_id

        return super().get_operation_id_base(path, method, action)
