from django.contrib import admin
from simple_history.admin import SimpleHistoryAdmin

from .models import (
    Address,
    Club,
    ContactMechanism,
    Country,
    Field,
    FieldTimeSlot,
    Match,
    MatchTeam,
    MatchTeamSeed,
    Order,
    OrderItem,
    Person,
    PhaseSeed,
    Product,
    Tournament,
    TournamentGroup,
    TournamentPhase,
    TournamentProduct,
    TournamentTeam,
    TournamentTeamPlayer,
    ZipCode,
)

"""
Automatic administrative interface to the models.

See: https://docs.djangoproject.com/en/4.0/ref/contrib/admin/
"""

# Register your models here.
admin.site.register(Address)
admin.site.register(Club)
admin.site.register(ContactMechanism)
admin.site.register(Country)
admin.site.register(Field)
admin.site.register(FieldTimeSlot)
admin.site.register(Match)
admin.site.register(MatchTeam)
admin.site.register(MatchTeamSeed)
admin.site.register(Person)
admin.site.register(Product)
admin.site.register(Order)
admin.site.register(OrderItem)
admin.site.register(PhaseSeed)
admin.site.register(Tournament)
admin.site.register(TournamentGroup)
admin.site.register(TournamentPhase)
admin.site.register(TournamentProduct)
admin.site.register(TournamentTeam)
admin.site.register(TournamentTeamPlayer)
admin.site.register(ZipCode)
