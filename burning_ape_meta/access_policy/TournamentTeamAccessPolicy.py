from rest_access_policy import AccessPolicy


class TournamentTeamAccessPolicy(AccessPolicy):
    statements = [
        {
            "action": ["list", "retrieve"],
            "principal": ["*"],
            "effect": "allow"
        },
        {
            "action": ["*"],
            "principal": ["admin"],
            "effect": "allow"
        },
    ]
