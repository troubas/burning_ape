from django.core.exceptions import ObjectDoesNotExist
from django.db.models import Q
from .PublicReadAccessPolicy import PublicReadAccessPolicy

from burning_ape_meta.models import (
    MatchTeam,
    TournamentTeamPlayer,
)


class MatchTeamAccessPolicy(PublicReadAccessPolicy):
    """
    Access policy for tournament teams

     * Everybody should be able to read.
    """
    statements = [
        {
            "action": ["list", "retrieve"],
            "principal": "*",
            "effect": "allow"
        },
        {
            "action": ["*"],
            "principal": ["*"],
            "effect": "allow"
        },
        {
            "action": ["update", "partial_update"],
            "principal": ["authenticated"],
            "effect": "allow",
            "condition": "is_opponent_team_member",
        },
    ]

    def is_opponent_team_member(self, request, view, action) -> bool:
        if request.user.is_staff or request.user.is_admin:
            return True

        instance = view.get_object()
        if (view.detail):
            if not isinstance(instance, MatchTeam):
                raise TypeError(instance)

            try:
                opponent = instance.left_team.right_team
            except ObjectDoesNotExist:
                opponent = instance.right_team.left_team

            pk = opponent.tournament_team

            return TournamentTeamPlayer.objects.filter(
                Q(tournament_team_id=pk), Q(player_id=person.pk)
            ).exists()
        else:
            return False
