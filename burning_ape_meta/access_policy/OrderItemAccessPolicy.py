from rest_access_policy import AccessPolicy


class OrderItemAccessPolicy(AccessPolicy):
    statements = [
        {
            "action": ["*"],
            "principal": ["admin"],
            "effect": "allow"
        },
    ]
