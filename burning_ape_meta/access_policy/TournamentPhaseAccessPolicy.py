from .PublicReadAccessPolicy import PublicReadAccessPolicy


class TournamentPhaseAccessPolicy(PublicReadAccessPolicy):
    """
    Access policy for phases

     * Everybody should be able to read.
    """
