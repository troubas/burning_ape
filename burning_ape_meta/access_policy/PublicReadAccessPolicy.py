from rest_access_policy import AccessPolicy


class PublicReadAccessPolicy(AccessPolicy):
    statements = [
        {
            "action": ["list", "retrieve"],
            "principal": "*",
            "effect": "allow"
        },
    ]
