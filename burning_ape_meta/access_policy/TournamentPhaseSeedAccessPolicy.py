from rest_access_policy import AccessPolicy


class TournamentPhaseSeedAccessPolicy(AccessPolicy):
    # TODO: Add owner
    statements = [
        {
            "action": ["*"],
            "principal": ["admin"],
            "effect": "allow"
        },
    ]
