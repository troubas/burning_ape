from rest_access_policy import AccessPolicy


class TournamentOrderStatsAccessPolicy(AccessPolicy):
    statements = [
        {
            "action": ["*"],
            "principal": ["*"],
            "effect": "allow"
        },
    ]
