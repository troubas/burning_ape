from rest_access_policy import AccessPolicy

from burning_ape_meta.models import Tournament


class TournamentAccessPolicy(AccessPolicy):
    """
    Access policy for tournaments

     * Everybody should be able to read.
     * Can be created by any authenticated user?
    """
    statements = [
        {
            "action": ["list", "retrieve"],
            "principal": "*",
            "effect": "allow"
        },
        {
            "action": ["*"],
            "principal": ["admin"],
            "effect": "allow"
        },
        {
            "action": ["*"],
            "principal": ["*"],
            "effect": "allow",
            "condition": "is_admin"
        },
    ]

    def check_admin(self, request, instance) -> bool:
        if not isinstance(instance, Tournament):
            raise TypeError(instance)

        return (instance.owner == request.user or
                instance.admins.filter(id=request.user.id).exists())

    def is_admin(self, request, view, action) -> bool:
        if (view.detail):
            return self.check_admin(request, view.get_object())
        else:
            return False

    @classmethod
    def scope_fields(cls, request, fields: dict, instance=None) -> dict:
        # never return admins/owner when retrieving a tournament list
        # only return admins/owner if the requesting user is admin or owner of
        # the specific tournament
        if (not isinstance(instance, Tournament)
                or not cls().check_admin(request, instance)):
            fields.pop('owner', None)
            fields.pop('admins', None)
        return fields
