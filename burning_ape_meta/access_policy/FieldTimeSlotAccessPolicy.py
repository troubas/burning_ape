from rest_access_policy import AccessPolicy


class FieldTimeSlotAccessPolicy(AccessPolicy):
    """
    Access policy for tournament fields

     * Everybody should be able to read.
    """
    statements = [
        {
            "action": ["list", "retrieve"],
            "principal": "*",
            "effect": "allow"
        },
    ]
