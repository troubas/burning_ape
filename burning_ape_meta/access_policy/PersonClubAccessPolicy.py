from rest_access_policy import AccessPolicy


class PersonClubAccessPolicy(AccessPolicy):
    # TODO: Add owner
    """
    Access policy for club membership
    """
    def __init__(self):
        self.statements += [
            {
                "action": ["*"],
                "principal": ["admin"],
                "effect": "allow"
            },
        ]
