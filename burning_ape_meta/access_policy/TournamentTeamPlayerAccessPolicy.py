from rest_access_policy import AccessPolicy


class TournamentTeamPlayerAccessPolicy(AccessPolicy):
    statements = [
        {
            "action": ["*"],
            "principal": ["*"],
            "effect": "allow"
        },
    ]
