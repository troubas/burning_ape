from rest_access_policy import AccessPolicy


class ZipCodeAccessPolicy(AccessPolicy):
    statements = [
        {
            "action": ["*"],
            "principal": ["admin"],
            "effect": "allow"
        },
    ]
