from rest_access_policy import AccessPolicy


class AddressAccessPolicy(AccessPolicy):
    # TODO: Add owner
    statements = [
        {
            "action": ["*"],
            "principal": ["admin"],
            "effect": "allow"
        },
    ]
