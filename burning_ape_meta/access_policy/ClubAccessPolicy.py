from .PublicReadAccessPolicy import PublicReadAccessPolicy


class ClubAccessPolicy(PublicReadAccessPolicy):
    # TODO: Add owner
    """
    Access policy for clubs

     * Everybody should be able to read.
     * Can be created by any authenticated user?
    """
    def __init__(self):
        self.statements += [
            {
                "action": ["*"],
                "principal": ["admin"],
                "effect": "allow"
            },
        ]
