from rest_access_policy import AccessPolicy


class PersonAccessPolicy(AccessPolicy):
    # TODO: a person may decide to hide properties of oneself.
    # We should at least allow an option if the name is public..
    # Everything else should be private by default.
    statements = [
        {
            "action": ["*"],
            "principal": ["admin"],
            "effect": "allow"
        },
    ]

    # Users can only view their own account
    @classmethod
    def scope_queryset(cls, request, qs):
        if (request.user.is_superuser):
            return qs

        return qs.filter(account=request.user)
