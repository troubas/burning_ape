from .PublicReadAccessPolicy import PublicReadAccessPolicy


class MatchAccessPolicy(PublicReadAccessPolicy):
    """
    Access policy for tournament fields

     * Everybody should be able to read.
    """
    statements = [
        {
            "action": ["list", "retrieve"],
            "principal": "*",
            "effect": "allow"
        },
    ]
