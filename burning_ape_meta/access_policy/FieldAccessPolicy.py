from rest_access_policy import AccessPolicy


class FieldAccessPolicy(AccessPolicy):
    statements = [
        {
            "action": ["list", "retrieve"],
            "principal": "*",
            "effect": "allow"
        },
        {
            "action": ["*"],
            "principal": ["admin"],
            "effect": "allow"
        },
    ]
