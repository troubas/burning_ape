from rest_access_policy import AccessPolicy


class PersonContactMechanismAccessPolicy(AccessPolicy):
    statements = [
        {
            "action": ["*"],
            "principal": ["admin"],
            "effect": "allow"
        },
    ]
