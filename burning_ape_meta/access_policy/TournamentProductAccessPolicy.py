from rest_access_policy import AccessPolicy

from burning_ape_meta.models import Tournament


class TournamentProductAccessPolicy(AccessPolicy):
    """
    Access policy for tournament products

     * Every registered user should be able to read.
     * Can be created by tournament owner/admins
    """
    statements = [
        {
            "action": ["list", "retrieve"],
            "principal": "*",
            "effect": "allow",
        },
        {
            "action": ["*"],
            "principal": ["admin"],
            "effect": "allow"
        },
        {
            "action": ["*"],
            "principal": ["*"],
            "effect": "allow",
            "condition": "is_admin"
        },
    ]

    def check_admin(self, request, instance) -> bool:
        if not isinstance(instance, Tournament):
            raise TypeError(instance)

        return (instance.owner == request.user or
                instance.admins.filter(id=request.user.id).exists())

    def is_admin(self, request, view, action) -> bool:
        if (view.detail):
            return self.check_admin(request, view.get_object())
        else:
            return False
