from rest_access_policy import AccessPolicy


class TournamentOrderAccessPolicy(AccessPolicy):
    statements = [
        {
            "action": ["*"],
            "principal": ["admin", "staff"],
            "effect": "allow"
        },
    ]
