from rest_access_policy import AccessPolicy


class OrderAccessPolicy(AccessPolicy):
    statements = [
        {
            "action": ["*"],
            "principal": ["admin"],
            "effect": "allow"
        },
    ]
