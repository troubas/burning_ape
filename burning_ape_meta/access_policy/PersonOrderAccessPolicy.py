from rest_access_policy import AccessPolicy


class PersonOrderAccessPolicy(AccessPolicy):
    statements = [
        {
            "action": ["*"],
            "principal": ["admin"],
            "effect": "allow"
        },
    ]
