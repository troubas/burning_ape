from django.urls import include, path, reverse
from rest_framework import status
from rest_framework.test import APITestCase, URLPatternsTestCase

import pdb


class MetaUrlPattern():
    urlpatterns = [
        path('meta/', include('burning_ape_meta.urls')),
    ]


class AdressTests(APITestCase, URLPatternsTestCase, MetaUrlPattern):

    def test_get_addresses_anonymous(self):
        """
        Try to get the address without being logged in
        """
        url = reverse('address-list')
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)


class ClubTests(APITestCase, URLPatternsTestCase, MetaUrlPattern):
    def test_get_clubs_anonymous(self):
        """
        Try to get the clubs without being logged in
        """
        url = reverse('club-list')
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)


class CountryTests(APITestCase, URLPatternsTestCase, MetaUrlPattern):
    def test_get_countries(self):
        """
        Ensure we can GET the list of countries
        """
        url = reverse('country-list')
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)


class FieldTests(APITestCase, URLPatternsTestCase, MetaUrlPattern):
    def test_get_fields(self):
        """
        Ensure we can GET the list of countries
        """
        url = reverse('field-list')
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)


class TournamentTests(APITestCase, URLPatternsTestCase, MetaUrlPattern):
    tournament_pk = 0

    def test_get_tournament_list(self):
        """
        Ensure we can GET the list of tournaments
        """
        url = reverse('tournament-list')
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_tournament(self):
        """
        GET a specific tournament
        """
        url = reverse('tournament-detail', args={'tournament_pk': 0})
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_get_match_list(self):
        url = reverse('match-list', args={'tournament_pk': 0})
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 0)

    def test_get_match(self):
        url = reverse('match-detail', args={'tournament_pk': 0, 'match_pk': 0})
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)


class PersonTests(APITestCase, URLPatternsTestCase, MetaUrlPattern):
    def test_get_persons(self):
        url = reverse('person-list')
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)


class ProductTests(APITestCase, URLPatternsTestCase, MetaUrlPattern):
    def test_get_products(self):
        url = reverse('product-list')
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)


class UserTests(APITestCase, URLPatternsTestCase, MetaUrlPattern):
    def test_get_users(self):
        url = reverse('user-list')
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)


class ZipCodeTests(APITestCase, URLPatternsTestCase, MetaUrlPattern):
    def test_get_zipcodes(self):
        url = reverse('zipcode-list')
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
