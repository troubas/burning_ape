import datetime

from django.core.exceptions import ValidationError
from django.contrib.auth.models import User
from django.db import (
    IntegrityError,
    transaction,
)
from django.test import TestCase
from faker import Faker
from faker.providers import (
    address,
    person
)

from burning_ape_meta.models import (
    Address,
    Club,
    ContactMechanism,
    Country,
    Field,
    Order,
    OrderItem,
    Person,
    PersonClub,
    Product,
    Tournament,
    Field,
    TournamentProduct,
    TournamentTeam,
    TournamentTeamPlayer,
    ZipCode,
)

import pdb

fake = Faker()
fake.add_provider(address)
fake.add_provider(person)

def generate_person():
    return Person.objects.create(
        first_name=fake.first_name()
    )


def generate_country():
    return Country.objects.create(
        country=fake.country(),
    )


def generate_zip_code():
    return ZipCode.objects.create(
        zip_code=fake.postcode(),
        locality=fake.city(),
        country=generate_country(),
    )


def generate_address():
    return Address.objects.create(
        line1=fake.street_name(),
        zip_code=generate_zip_code(),
    )


def generate_club():
    return Club.objects.create(
        name=fake.company(),
        provenance=generate_address(),
    )


def generate_field():
    try:
        generate_field.counter += 1
    except AttributeError:
        generate_field.counter = 1

    return Field.objects.create(
        name=' '.join(['Field', str(generate_field.counter)]),
        location=generate_address(),
    )


def generate_order():
    return Order.objects.create(
        person=generate_person(),
        tournament=generate_tournament(),
    )


def generate_product():
    return Product.objects.create(
        product_name=fake.color_name(),
        price=fake.numerify(),
    )


def generate_tournament():
    try:
        generate_tournament.counter += 1
    except AttributeError:
        generate_tournament.counter = 1

    return Tournament.objects.create(
        date=fake.date(),
        location=generate_address(),
        name=' '.join(['Tournament', str(generate_tournament.counter)]),
        owner=generate_user()
    )


def generate_user():
    return User.objects.create(
        username=fake.user_name(),
    )


class AddressModelTests(TestCase):
    """
    Test cases for :class:`Address`
    """
    #populator = Faker.getPopulator()

    def test_empty_address(self):
        """
        This is nonsense and thus should be impossible
        """
        self.assertRaises(IntegrityError, Address.objects.create)

    def test_duplicate_address(self):
        """
        Prevent two addresses with exact same information
        """
        z = generate_zip_code()
        addr = Address(
            zip_code=z
        )
        addr.save()
        addr1 = Address(
            zip_code=z
        )
        self.assertRaises(IntegrityError, addr1.save)

    def test_create_address(self):
        addr = generate_address()
        self.assertIsNotNone(addr.pk)


class ClubModelTests(TestCase):
    """
    Test cases for :class:`Club`
    """

    def setUp(self):
        self.address = generate_address()

    def test_create_club_without_name(self):
        # TODO: fix model
        club = Club(
            provenance=self.address,
        )
        self.assertRaises(ValidationError, club.full_clean)

    def test_create_club_without_provenance(self):
        club = Club(name='test')
        self.assertRaises(IntegrityError, club.save)

    def test_create_club(self):
        club = Club.objects.create(
            provenance=self.address,
            name='Skurr',
        )
        self.assertIsNotNone(club.pk)

    def test_create_duplicate_club(self):
        """
        create two clubs with equal name/provenance
        """
        n = 'Skurr'
        Club.objects.create(
            provenance=self.address,
            name=n,
        )
        club2 = Club(
            provenance=self.address,
            name=n,
        )
        self.assertRaises(IntegrityError, club2.save)


class CountryModelTest(TestCase):

    def test_create_empty_country(self):
        c = Country()
        self.assertRaises(ValidationError, c.full_clean)

    def test_create_country(self):
        country = generate_country()
        self.assertIsNotNone(country.pk)

    def test_create_duplicate_country(self):
        _ = Country.objects.create(
            country="Schweiz"
        )
        country2 = Country(
            country="Schweiz",
        )
        self.assertRaises(IntegrityError, country2.save)


class FieldModelTest(TestCase):

    def test_create_field(self):
        field = generate_field()
        self.assertIsNotNone(field)

    def test_create_field_without_location(self):
        self.assertRaises(IntegrityError, lambda: Field.objects.create(name='Field 1'))


class OrderModelTest(TestCase):

    def test_create_order_without_person(self):
        # TODO: fix model
        self.assertRaises(
            IntegrityError,
            lambda: Order.objects.create(
                tournament=generate_tournament()
            ))

    def test_create_order(self):
        order = generate_order()
        self.assertIsNotNone(order.pk)


class OrderItemTest(TestCase):

    def test_create_order_item_without_order(self):
        self.assertRaises(
            IntegrityError,
            lambda: OrderItem.objects.create(
                product=generate_product(),
                quantity=1,
                price=1,
            ))

    def test_create_order_item_without_product(self):
        self.assertRaises(
            IntegrityError,
            lambda: OrderItem.objects.create(
                order=generate_order(),
                quantity=1,
                price=1,
            ))

    def test_create_order_item_without_quantity(self):
        self.assertRaises(
            IntegrityError,
            lambda: OrderItem.objects.create(
                order=generate_order(),
                product=generate_product(),
                price=1,
            ))

    def test_create_order_item_without_price(self):
        self.assertRaises(
            IntegrityError,
            lambda: OrderItem.objects.create(
                order=generate_order(),
                product=generate_product(),
                quantity=1,
            ))

    def test_create_order_item(self):
        order_item = OrderItem.objects.create(
            order=generate_order(),
            product=generate_product(),
            quantity=1,
            price=1,
        )
        self.assertIsNotNone(order_item.pk)


class PlayerModelTests(TestCase):

    def test_minimal_player_information(self):
        """
        We're happy if we have a first name for a person.
        """
        player = generate_person()
        self.assertIsNotNone(player.pk)
        player.delete()
        self.assertIsNone(player.pk)


class ZipCodeTests(TestCase):

    def test_create_empty_zip_code(self):
        self.assertRaises(IntegrityError, ZipCode.objects.create)

    def test_create_zip_code_without_country(self):
        zip_code = ZipCode(
            zip_code="3000",
            locality="Bern",
        )
        self.assertRaises(IntegrityError, zip_code.save)

    def test_create_zip_code_without_zip_code(self):
        zip_code = ZipCode(
            locality="Bern",
            country=generate_country(),
        )
        self.assertRaises(ValidationError, zip_code.full_clean)

    def test_create_zip_code(self):
        zip_code = generate_zip_code()
        self.assertIsNotNone(zip_code.pk)
