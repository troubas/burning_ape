from django.apps import AppConfig


class BurningApeMetaConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'burning_ape_meta'
