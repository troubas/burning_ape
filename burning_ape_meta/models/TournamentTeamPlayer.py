from django.db import models
from simple_history.models import HistoricalRecords
from .Person import Person
from .TournamentTeam import TournamentTeam


class TournamentTeamPlayer(models.Model):
    """
    Tournament Specific Team

    Because a :class:`player` can play in different teams for each
    :class:`Tournament` we must have tournament-specific teams.
    """

    tournament_team: models.ForeignKey = models.ForeignKey(
        TournamentTeam,
        on_delete=models.CASCADE,
        related_name='tournament_team_players',
    )

    player: models.ForeignKey = models.ForeignKey(
        Person,
        on_delete=models.PROTECT,
        related_name='tournament_team_players',
    )

    number: models.CharField = models.CharField(
        max_length=4,
        null=False,
        blank=True,
        default="",
    )

    history = HistoricalRecords()

    class Meta:
        unique_together = [['tournament_team', 'player']]

    @property
    def public_name(self) -> str:
        """Returns the person's choice of public display name."""
        return self.player.public_name

    def __str__(self):
        return ", ".join([self.player.full_name, self.tournament_team.team_name])
