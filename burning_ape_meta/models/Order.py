from django.db import models
from django.db.models import Sum, F
from simple_history.models import HistoricalRecords
from .Person import Person
from .Tournament import Tournament


class Order(models.Model):
    """
    An order.
    """


    person: models.ForeignKey = models.ForeignKey(
        Person,
        on_delete=models.PROTECT,
        null=False,
        related_name='orders'
    )
    """
    Ways of contact
    """

    order_datetime: models.DateTimeField = models.DateTimeField(
        auto_now=True,
        blank=True
    )
    """
    When was the order issued
    """

    tournament: models.ForeignKey = models.ForeignKey(
        Tournament,
        on_delete=models.PROTECT
    )

    history = HistoricalRecords()

    @property
    def person_name(self) -> str:
        """Returns the calculated price (quantity * price)"""
        return self.person.full_name

    @property
    def total_price(self) -> float:
        """Returns the calculated price (quantity * price)"""
        return self.order_items.aggregate(sum=Sum(F('price') * F('quantity')))['sum']

    def __str__(self):
        return ": ".join([str(self.pk), self.person.full_name])
