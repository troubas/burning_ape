from django.db import models

from .MatchTeamSeed import MatchTeamSeed
from .TournamentTeam import TournamentTeam


class MatchTeam(models.Model):
    """
    """

    class SpiritScore(models.IntegerChoices):
        Excellent = 4
        VeryGood = 3
        Good = 2
        NotGood = 1
        Poor = 0

    score: models.IntegerField = models.IntegerField(
        null=True,
        blank=True,
    )

    rules_knowledge_and_use: models.IntegerField = models.IntegerField(
        choices=SpiritScore.choices,
        null=True,
        blank=True,
    )

    fouls_and_body_contact: models.IntegerField = models.IntegerField(
        choices=SpiritScore.choices,
        null=True,
        blank=True,
    )

    fair_mindedness: models.IntegerField = models.IntegerField(
        choices=SpiritScore.choices,
        null=True,
        blank=True,
    )

    attitude_and_self_control: models.IntegerField = models.IntegerField(
        choices=SpiritScore.choices,
        null=True,
        blank=True,
    )

    communication: models.IntegerField = models.IntegerField(
        choices=SpiritScore.choices,
        null=True,
        blank=True,
    )

    tournament_team: models.ForeignKey = models.ForeignKey(
        to=TournamentTeam,
        on_delete=models.PROTECT,
        null=True,
    )

    spirit_remarks: models.CharField = models.CharField(
        max_length=2048,
        null=True,
        blank=True,
    )

    placeholder: models.CharField = models.CharField(
        max_length=128,
        null=False,
        blank=True,
        default='',
    )

    seed: models.OneToOneField = models.OneToOneField(
        to=MatchTeamSeed,
        on_delete=models.CASCADE,
    )

    @property
    def get_display_name(self):
        if (self.tournament_team is None):
            return str(self.seed)
        else:
            return self.tournament_team.team_name

    def __str__(self) -> str:
        if (self.tournament_team is None):
            return str(self.seed)
        else:
            return self.tournament_team.team_name
