from datetime import datetime
from django.db import models
from django.db.models import Q

from .FieldTimeSlot import FieldTimeSlot
from .MatchTeam import MatchTeam
from .Tournament import Tournament


class Match(models.Model):
    """
    A match is played between two teams
    """

    tournament: models.ForeignKey = models.ForeignKey(
        to=Tournament,
        on_delete=models.CASCADE,
    )
    """
    Part of which tournament
    """

    name: models.CharField = models.CharField(
        max_length=128,
        null=False,
    )
    """
    Each match requires a name that is unique per tournament.

    A possible default implementation for the name of a new match would be the amount of
    matches for a tournament + 1.
    """

    left_team: models.OneToOneField = models.OneToOneField(
        to=MatchTeam,
        on_delete=models.CASCADE,
        related_name='left_team',
    )
    """
    team 1
    """

    right_team: models.OneToOneField = models.OneToOneField(
        to=MatchTeam,
        on_delete=models.CASCADE,
        related_name='right_team',
    )
    """
    team 2
    """

    field_time_slot: models.OneToOneField = models.OneToOneField(
        to=FieldTimeSlot,
        on_delete=models.PROTECT,
        null=True,
        blank=True,
    )
    """
    """

    allow_draw: models.BooleanField = models.BooleanField(
        default=False,
    )
    """
    """

    played: models.BooleanField = models.BooleanField(
        default=False,
    )
    """
    """

    @property
    def match_start(self) -> datetime:
        return self.field_time_slot.start_time

    @property
    def match_end(self) -> datetime:
        return self.field_time_slot.end_time

    @property
    def field_name(self) -> str:
        return self.field_time_slot.field.name

    @property
    def can_get_result(self):
        if not self.played or self.left_team is None or self.right_team is None:
            return False
        if self.allow_draw and self.left_team.score == self.right_team.score:
            return False

        if not (
            self.left_team.score
            and self.right_team.score
        ):
            return False

        return True

    @property
    def winner(self):
        if not self.can_get_result:
            return None

        if self.left_team.score > self.right_team.score:
            return self.left_team
        else:
            return self.right_team

    @property
    def loser(self):
        if not self.can_get_result:
            return None

        if self.left_team.score < self.right_team.score:
            return self.left_team
        else:
            return self.right_team

    class Meta:
        models.UniqueConstraint(
            fields=['tournament_id', 'name'],
            name='unique_match_name_per_tournament',
        )
        models.CheckConstraint(
            check=Q(left_team_id__neq='right_team_id'),
            name='check_match_between_different_teams',
        )

    # TODO:
    def __str__(self) -> str:
        match (self.left_team_id, self.right_team_id):
            case (None, None):
                return "{}: {} vs {}".format(self.pk, "?", "?")
            case (_, None):
                return "{}: {} vs {}".format(self.pk, self.left_team, "?")
            case (None, _):
                return "{}: {} vs {}".format(self.pk, "?", self.right_team)
            case _:
                return "{}: {} vs {}".format(self.pk, self.left_team, self.right_team)
