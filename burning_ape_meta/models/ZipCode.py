from django.core.validators import MinLengthValidator
from django.db import models
from simple_history.models import HistoricalRecords
from .Country  import Country


class ZipCode(models.Model):
    """
    Practically all countries are subdivided by zip-codes.

    Usually consits of a number or alphanumeric identifier and a name.
    """

    country: models.ForeignKey = models.ForeignKey(
        Country,
        on_delete=models.PROTECT
    )
    """
    Zip-Code of which county?
    """

    zip_code: models.CharField = models.CharField(
        max_length=32,
        null=False,
        validators=[MinLengthValidator(2)],
    )
    """
    Alphanumeric identifier?
    """

    locality: models.CharField = models.CharField(
        max_length=256,
    )
    """
    Name of the place.
    """

    history = HistoricalRecords()

    @property
    def country_name(self) -> str:
        """Get the name of the countrty"""
        return self.country.country

    class Meta:
        unique_together = [['country', 'zip_code', 'locality']]
        ordering = ["zip_code", "locality"]

    def __str__(self):
        return ", ".join([self.zip_code, self.locality])
