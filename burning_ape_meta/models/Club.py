from django.core.validators import MinLengthValidator
from django.db import models
from simple_history.models import HistoricalRecords

from .Address import Address
from .ContactMechanism import ContactMechanism
from .PersonClub import PersonClub


class Club(models.Model):
    """
    A Club describes a :class:`Team` that exists outside the scope of a tournament.

    It can be used for people that often play together to build a reusable roster.
    """

    contact_mechanism: models.ManyToManyField = models.ManyToManyField(
        ContactMechanism,
    )
    """
    Ways of contact
    """

    name: models.CharField = models.CharField(
        max_length=128,
        null=False,
        validators=[MinLengthValidator(2)],
    )
    """
    What's the team called?
    """

    provenance: models.ForeignKey = models.ForeignKey(
        Address,
        on_delete=models.PROTECT,
        null=False,
    )
    """
    Where's the team from?
    """

    players: models.ManyToManyField = models.ManyToManyField(
        'Person',
        through=PersonClub,
    )

    history = HistoricalRecords()
    """
    Access to the audit log created by simple_history
    """

    class Meta:
        unique_together = [['name', 'provenance']]

    def __str__(self) -> str:
        """
        Returns a unique string representation of a :class:`Team`.
        """
        return " ".join([
            self.name,
            "".join(["(", str(self.provenance), ")"])
        ])
