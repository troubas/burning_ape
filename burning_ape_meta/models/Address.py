from django.db import models
from simple_history.models import HistoricalRecords
from .ZipCode import ZipCode


class Address(models.Model):
    """
    """

    line1: models.CharField = models.CharField(
        max_length=256,
        null=False,
        blank=True,
        default='',
    )
    """
    First line
    """

    line2: models.CharField = models.CharField(
        max_length=256,
        null=False,
        blank=True,
        default='',
    )
    """
    Second line
    """

    line3: models.CharField = models.CharField(
        max_length=256,
        null=False,
        blank=True,
        default='',
    )
    """
    Third line
    """

    line4: models.CharField = models.CharField(
        max_length=256,
        null=False,
        blank=True,
        default='',
    )
    """
    Fourth line
    """

    zip_code: models.ForeignKey = models.ForeignKey(
        ZipCode,
        on_delete=models.PROTECT
    )
    """
    Zip Code
    """

    history = HistoricalRecords()

    class Meta:
        unique_together = [[
            'line1',
            'line2',
            'line3',
            'line4',
            'zip_code',
        ]]

    def __str__(self) -> str:
        addr_line = []
        (addr_line.append(self.line1) if self.line1 else None)
        (addr_line.append(self.line2) if self.line2 else None)
        (addr_line.append(self.line3) if self.line3 else None)
        (addr_line.append(self.line4) if self.line4 else None)
        addr_line.append(" ".join([
            self.zip_code.zip_code,
            self.zip_code.locality,
        ]))
        addr_line.append(self.zip_code.country.country)

        return ", ".join(addr_line)
