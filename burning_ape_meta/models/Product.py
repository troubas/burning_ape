from django.db import models
from simple_history.models import HistoricalRecords


class Product(models.Model):
    """
    Service rendered or product sold.
    """

    class Category(models.TextChoices):
        Food = 'Food'
        Drinks = 'Drinks'
        Merchandise = 'Merch'

    product_name: models.CharField = models.CharField(
        unique=True,
        max_length=64
    )

    price: models.FloatField = models.FloatField(
        max_length=16,
        default=0
    )

    category: models.CharField = models.CharField(
        max_length=6,
        choices=Category.choices,
        null=True,
        blank=True,
    )

    history = HistoricalRecords()

    def __str__(self):
        return self.product_name
