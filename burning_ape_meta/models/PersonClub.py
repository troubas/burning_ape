from django.db import models
from simple_history.models import HistoricalRecords


class PersonClub(models.Model):
    """
    """

    person: models.ForeignKey = models.ForeignKey(
        to='Person',
        on_delete=models.CASCADE
    )

    club: models.ForeignKey = models.ForeignKey(
        to='Club',
        on_delete=models.CASCADE
    )

    date_from: models.DateField = models.DateField(
        auto_now=True
    )

    date_until: models.DateField = models.DateField(
        null=True,
        blank=True
    )

    player_number: models.CharField = models.CharField(
        max_length=8,
        null=False
    )
    """
    Number on the shirt. Might change from club to club?
    """

    history = HistoricalRecords()
