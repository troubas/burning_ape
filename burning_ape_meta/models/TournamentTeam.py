from django.db import models
from django.db.models.fields import related
from simple_history.models import HistoricalRecords
from .Person import Person
from .Tournament import Tournament


class TournamentTeam(models.Model):
    """
    A collection of players that attend a tournament.

    Teams are used as the basis to create a game-plan. Each team consists out of any
    number of players.
    """

    team_name: models.CharField = models.CharField(
        max_length=128,
        null=False,
    )

    tournament: models.ForeignKey = models.ForeignKey(
        Tournament,
        related_name='teams',
        on_delete=models.CASCADE
    )

    contact: models.ForeignKey = models.ForeignKey(
        Person,
        on_delete=models.PROTECT
    )

    registration_date: models.DateTimeField = models.DateTimeField()

    history = HistoricalRecords()

    class Meta:
        unique_together = [['tournament', 'team_name']]

    def __str__(self):
        return "{}, {}".format(
            self.tournament,
            self.team_name,
        )
