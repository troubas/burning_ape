from django.db import models

from .TournamentPhase import TournamentPhase


class TournamentGroup(models.Model):
    """
    """

    name: models.CharField = models.CharField(
        max_length=128,
    )
    """
    """

    short_name: models.CharField = models.CharField(
        max_length=4,
    )
    """
    """

    phase: models.ForeignKey = models.ForeignKey(
        to=TournamentPhase,
        on_delete=models.CASCADE,
        null=False,
        related_name='groups',
    )

    def __str__(self):
        return self.name
