from django.db import models

from .Field import Field


class FieldTimeSlot(models.Model):
    """
    Every field has time slots when it's possible to play on them. The time
    slot can be used for exactly one Match.
    """

    start_time: models.DateTimeField = models.DateTimeField(
    )

    end_time: models.DateTimeField = models.DateTimeField(
    )

    field: models.ForeignKey = models.ForeignKey(
        to=Field,
        on_delete=models.CASCADE,
    )
