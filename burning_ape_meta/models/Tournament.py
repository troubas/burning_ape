from django.contrib.auth.models import User
from django.db import models
from simple_history.models import HistoricalRecords

from .Address import Address
from .ContactMechanism import ContactMechanism
from .Field import Field
from .Product import Product


class Tournament(models.Model):
    """
    Tournament
    """

    class Division(models.TextChoices):
        Men = 'Men'
        Mixed = 'Mixed'
        Open = 'Open'
        Woman = 'Woman'

    class GameMode(models.TextChoices):
        SwissDraw = 'Swiss Draw'
        RoundRobin = 'Round Robin'

    contact_mechanism: models.ManyToManyField = models.ManyToManyField(
        to=ContactMechanism,
    )
    """
    Ways of contact
    """

    date: models.DateField = models.DateField()
    """
    When is the tournament?
    """

    products: models.ManyToManyField = models.ManyToManyField(
        to=Product,
        through='TournamentProduct'
    )
    """
    Which products are available?
    """

    game_mode: models.CharField = models.CharField(
        max_length=6,
        choices=Division.choices,
        null=True,
        blank=True,
    )
    """
    What's being played?
    """

    location: models.ForeignKey = models.ForeignKey(
        to=Address,
        on_delete=models.PROTECT,
    )
    """
    Where is the tournament?
    """

    name: models.CharField = models.CharField(
        max_length=128,
    )
    """
    How's the tournament called?
    """

    owner: models.ForeignKey = models.ForeignKey(
        to=User,
        on_delete=models.PROTECT,
        null=False,
    )
    """
    Who's the owner of the tournament.

    The only one that can delete it as well?  Per default gets set to the creator.
    Can not be null?

    If a user deletes his account ownership needs to be transfered or the
    tournament deleted?
    """

    admins: models.ManyToManyField = models.ManyToManyField(
        to=User,
        related_name="tournament_admin",
    )
    """
    List of tournament administrators.
    """

    history = HistoricalRecords()
    """
    Historical data (django-simple-history)
    """

    def __str__(self):
        return self.name
