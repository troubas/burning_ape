from django.db import models

from .TournamentGroup import TournamentGroup
from .TournamentTeam import TournamentTeam
from .TournamentPhase import TournamentPhase


class PhaseSeed(models.Model):
    """
    Seeding for a phase consists of an ordered list of teams.

    * The first Phase of a tournament is seeded by either a list provided by
      the user or randomly based on the partitioning teams.
    * The following Phases of a tournament are seeded by the previous phase.

    Before a phase can start the PhaseSeed has to be populated. However the
    PhaseSeed is initialized with empty data to allow planning of the
    tournament in advance.
    """

    rank: models.IntegerField = models.IntegerField(
        null=False,
    )

    team: models.ForeignKey = models.ForeignKey(
        to=TournamentTeam,
        on_delete=models.PROTECT,
        null=True,
    )

    phase: models.ForeignKey = models.ForeignKey(
        to=TournamentPhase,
        on_delete=models.CASCADE,
        related_name='phaseseed',
        null=False,
    )

    group: models.ForeignKey = models.ForeignKey(
        to=TournamentGroup,
        on_delete=models.SET_NULL,
        null=True,
    )
    """
    """

    class Meta():
        unique_together = [
            ['phase', 'rank'],
            ['phase', 'team'],
        ]

    def __str__(self) -> str:
        if self.team is not None:
            return str(self.team)
        else:
            return "{}, Seed #{}".format(
                self.phase,
                self.rank,
            )
