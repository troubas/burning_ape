from django.db import models
from simple_history.models import HistoricalRecords
from .Address import Address


class Field(models.Model):
    """
    Playing field
    """

    name: models.CharField = models.CharField(
        max_length=128,
        null=False
    )
    """
    How's the field called?
    """

    width: models.IntegerField = models.IntegerField(
        null=True,
        blank=True,
    )
    length: models.IntegerField = models.IntegerField(
        null=True,
        blank=True,
    )

    location: models.ForeignKey = models.ForeignKey(
        Address,
        on_delete=models.PROTECT,
    )

    history = HistoricalRecords()
    """
    Historical data (django-simple-history)
    """

    class Meta:
        unique_together = [['name', 'location']]

    def __str__(self):
        return ", ".join([self.name, str(self.location)])
