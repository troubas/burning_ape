from django.core.validators import MinLengthValidator
from django.db import models
from simple_history.models import HistoricalRecords


class Country(models.Model):
    """
    Distinct part of the world, such as a state or a nation.
    """

    country: models.CharField = models.CharField(
        max_length=128,
        unique=True,
        null=False,
        validators=[MinLengthValidator(2)],
    )

    history = HistoricalRecords()

    def __str__(self):
        return self.country
