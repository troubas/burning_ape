from django.db import models
from django.db.models import Q

from .PhaseSeed import PhaseSeed


class MatchTeamSeed(models.Model):
    """
    Seeding of matches.

    * A match can be seeded by matches from a previous phase.  Where either
      per-group ranking or global ranking will decide against whom the next
      match will be played.
    * A match can be seeded by another match (bracket mode).

    To enforce uniqueness only one can be defined.
    """

    seed_from_match: models.ForeignKey = models.ForeignKey(
        to='Match',
        on_delete=models.CASCADE,
        null=True,
        related_name='matchteamseed',
    )
    """
    Reference to match that is used as seeding basis. Because during planing
    phase the winner of the match is unknown it has to be determined after the
    match is played.
    """

    seed_from_winner: models.BooleanField = models.BooleanField(
        default=True
    )
    """
    If true picks the winner from a match as the seed. If false picks the loser
    of a match as seed.
    """

    seed_from_ranking: models.ForeignKey = models.ForeignKey(
        to=PhaseSeed,
        on_delete=models.CASCADE,
        related_name="matchteamseed",
        null=True,
    )
    """
    Reference an entry in the initial phase seeding.
    """

    placeholder: models.CharField = models.CharField(
        max_length=128,
        null=False,
        blank=True,
        default="",
    )
    """
    Placeholder for human identification of the seed
    """

    class Meta:
        constraints = [
            # Enforce exactly one of match or rank seeding
            models.CheckConstraint(
                check=(Q(seed_from_match__isnull=False, seed_from_ranking=None)
                       | Q(seed_from_match=None, seed_from_ranking__isnull=False)),
                name='define_exactly_one_seed_from_field'
            )
        ]

    def __str__(self) -> str:
        if self.seed_from_match is not None:
            m = self.seed_from_match
            if self.seed_from_winner:
                return "{} {}".format("Winner of ", m.name)
            else:
                return "{} {}".format("Loser of ", m.name)
        else:
            if (self.placeholder):
                return self.placeholder
            return str(self.seed_from_ranking)
