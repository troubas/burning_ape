from django.db import models
from simple_history.models import HistoricalRecords


class ContactMechanism(models.Model):
    """
    Way of contact.
    """

    class ContactType(models.TextChoices):
        """
        How can we get in contact?
        """
        Mail = 'Email'
        Phone = 'Phone'
        Letter = 'Letter'

    contact_type: models.CharField = models.CharField(
        max_length=6,
        choices=ContactType.choices,
    )
    """
    See :class:`ContactType`
    """

    email: models.CharField = models.CharField(
        max_length=1024,
        null=True,
        blank=True,
    )
    """
    """

    phone_number: models.CharField = models.CharField(
        max_length=15,
        null=True,
        blank=True,
    )
    """
    """

    address: models.ForeignKey = models.ForeignKey(
        'Address',
        on_delete=models.PROTECT,
        null=True,
        blank=True,
    )
    """
    """

    validated: models.BooleanField = models.BooleanField(
        default=False
    )

    history = HistoricalRecords()

    def __str__(self):
        match self.contact_type:
            case self.ContactType.Mail:
                return "%s: %s" % (self.contact_type, self.email)
            case self.ContactType.Phone:
                return "%s: %s" % (self.contact_type, self.phone_number)
            case self.ContactType.Letter:
                return "%s: %s" % (self.contact_type, str(self.address))
            case _:
                return ""
