from django.db import models
from .Tournament import Tournament


class TournamentPhase(models.Model):
    """
    """

    class GameMode(models.TextChoices):
        SwissDraw = 'Swiss Draw'
        RoundRobin = 'Round Robin'
        Knockout = 'Knockout'

    name: models.CharField = models.CharField(
        max_length=128
    )
    """
    Name of the pase
    """

    game_mode: models.CharField = models.CharField(
        max_length=16,
        choices=GameMode.choices,
        null=False,
        blank=False,
    )

    tournament: models.ForeignKey = models.ForeignKey(
        to=Tournament,
        on_delete=models.CASCADE,
        null=False,
        related_name='phases',
    )
    """
    """

    size: models.PositiveSmallIntegerField = models.PositiveSmallIntegerField()
    """
    Amount of teams allowed for the phase.
    """

    phase_number: models.PositiveSmallIntegerField = models.PositiveSmallIntegerField()
    """
    Keep phases ordered.
    """

    def __str__(self) -> str:
        return "Phase {}".format(
            self.phase_number,
        )

    class Meta:
        unique_together = [['tournament', 'phase_number']]
