from django.db import models
from simple_history.models import HistoricalRecords
from .Order import Order
from .Product import Product


class OrderItem(models.Model):
    """
    Part of an order.
    """

    order: models.ForeignKey = models.ForeignKey(
        Order,
        models.CASCADE,
        related_name='order_items',
    )

    product: models.ForeignKey = models.ForeignKey(
        Product,
        models.PROTECT,
    )

    @property
    def product_name(self) -> str:
        """Returns the product's name."""
        return self.product.product_name

    price: models.FloatField = models.FloatField(
        max_length=16,
    )

    quantity: models.IntegerField = models.IntegerField()

    history = HistoricalRecords()

    def __str__(self):
        return ": ".join([
            str(self.order.pk),
            " ".join([str(self.quantity), self.product.product_name])
        ])
