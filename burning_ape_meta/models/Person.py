from datetime import datetime
from dateutil import relativedelta
from django.contrib.auth.models import User
from django.db import models
from simple_history.models import HistoricalRecords
from typing import Optional

from .ContactMechanism import ContactMechanism
from .Club import Club
from .PersonClub import PersonClub


class Person(models.Model):
    """
    A physical person (a.k.a human)
    """

    account: models.OneToOneField = models.OneToOneField(
        to=User,
        on_delete=models.DO_NOTHING,
        null=True,
        blank=True,
        related_name='person',
    )
    """
    Account of the person.

     * It's possible that a person has no account (registered by team captain)
     * It should be possible to create an account for an existing person in the frontend
     * Deleting an account does not delete the person
    """

    birthday: models.DateField = models.DateField(null=True, blank=True)
    """
    The :attr:`birthday` can be used to authenticate age.

    Since players can enter their birhtday themselves the :attr:`birthday_validated` field
    must be set once by an authorized entity.

    If the player changes their birthday after validation the :attr:`birthday_validated`
    field must be reset.
    """

    # TODO: reset when birthday changes
    birthday_validated: models.BooleanField = models.BooleanField(
        default=False,
        blank=True
    )
    """
    The birthday of a player must be validated before it can be used for age verification.
    """

    contact_mechanism: models.ManyToManyField = models.ManyToManyField(
        ContactMechanism,
        blank=True
    )
    """
    Ways of contact
    """

    first_name: models.CharField = models.CharField(
        max_length=128,
        null=False
    )
    """
    First name (feel free to place a nickname).
    """

    last_name: models.CharField = models.CharField(
        max_length=128,
        null=False,
        blank=True,
        default=""
    )
    """
    Last name of the player.

    The field may be left empty. However the combination of
    first_name/last_name should be unique per team.
    """

    clubs: models.ManyToManyField = models.ManyToManyField(
        Club,
        through=PersonClub
    )
    """
    To which :class:`Club` does the :class:`Person` belong?
    """

    tournament_team: models.ManyToManyField = models.ManyToManyField(
        'TournamentTeam',
        through='TournamentTeamPlayer'
    )
    """
    Associate the :class:`Player` with a :class:`TournamentTeam`

    Basically this will provide a list of tournaments in which the player has
    participated as part of a specific team.
    """

    history = HistoricalRecords()

    @property
    def full_name(self) -> str:
        """Returns the person's full name."""
        return '%s %s' % (self.first_name, self.last_name)

    @property
    def public_name(self) -> str:
        """Returns the person's choice of public display name."""
        return '%s %s' % (self.first_name, self.last_name)

    @property
    def age(self) -> Optional[int]:
        if (self.birthday):
            return relativedelta.relativedelta(datetime.now().date(), self.birthday).years
        else:
            return None

    def __str__(self):
        return " ".join([self.first_name, self.last_name])
