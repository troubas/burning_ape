from django.db import models
from simple_history.models import HistoricalRecords
from .Tournament import Tournament
from .Product import Product


class TournamentProduct(models.Model):
    """
    Tournament-Specific products

    (all products should be tournament specific?)

    Basically this table helps to determine which products are available.
    """

    tournament: models.ForeignKey = models.ForeignKey(
        Tournament,
        on_delete=models.PROTECT
    )

    product: models.ForeignKey = models.ForeignKey(
        Product,
        on_delete=models.PROTECT
    )

    @property
    def price(self) -> float:
        """Returns the price of the product"""
        return self.product.price

    @property
    def product_name(self) -> str:
        """Returns the name of the product"""
        return self.product.product_name

    @property
    def category(self) -> str:
        """Returns the category of the product"""
        return self.product.category

    history = HistoricalRecords()

    def __str__(self):
        return ": ".join([
            self.tournament.name,
            self.product.product_name
        ])
