from burning_ape_meta.models.Order import Order
from burning_ape_meta.models.Person import Person
from decimal import Decimal
from django.db import models
from django.db.models import DecimalField, ExpressionWrapper, F, Sum
from django.db.models.query import QuerySet


class OrderQuerySet(models.QuerySet):

    def get_persons_orders(self, persons: list[Person]) -> QuerySet:
        return Order.objects.filter(
            person__in=persons
        ).all()

    def get_total_price_for_persons(
        self, persons: list[Person]
    ) -> Decimal:
        price = self.get_persons_orders(persons).annotate(
            total_price=ExpressionWrapper(
                F('order_items__price') * F('order_items__quantity'),
                output_field=DecimalField())
        ).aggregate(sum=Sum('total_price'))['sum']

        return price if price else 0.0
