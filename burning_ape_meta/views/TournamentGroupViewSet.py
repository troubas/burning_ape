from rest_framework import (
    viewsets,
)

from burning_ape_meta.access_policy import (
    TournamentGroupAccessPolicy,
)
from burning_ape_meta.serializers import (
    TournamentGroupSerializer,
)
from burning_ape_meta.models import (
    TournamentGroup,
)


class TournamentGroupViewSet(viewsets.ModelViewSet):
    permissions = [TournamentGroupAccessPolicy]
    queryset = TournamentGroup.objects.all()
    serializer_class = TournamentGroupSerializer
