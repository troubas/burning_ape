from rest_framework import (
    viewsets,
)

from burning_ape_meta.access_policy import (
    TournamentPhaseAccessPolicy,
)
from burning_ape_meta.serializers import (
    TournamentPhaseSerializer,
)
from burning_ape_meta.models import (
    TournamentPhase,
)


class TournamentPhaseViewSet(viewsets.ModelViewSet):
    permission_classes = [TournamentPhaseAccessPolicy]
    queryset = TournamentPhase.objects.all()
    serializer_class = TournamentPhaseSerializer

    def get_queryset(self):
        return (self
                .queryset
                .filter(tournament_id=self.kwargs['tournament_pk'])
                .order_by('phase_number'))
