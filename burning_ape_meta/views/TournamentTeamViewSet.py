from django.db.models import Prefetch

from rest_framework import (
    viewsets,
)

from burning_ape_meta.access_policy import (
    TournamentTeamAccessPolicy,
)
from burning_ape_meta.serializers import (
    TournamentTeamSerializer,
)
from burning_ape_meta.models import (
    TournamentTeam,
    TournamentTeamPlayer,
)


class TournamentTeamViewSet(viewsets.ModelViewSet):
    permission_classes = [TournamentTeamAccessPolicy]
    queryset = TournamentTeam.objects.order_by('team_name').all()
    serializer_class = TournamentTeamSerializer

    def get_queryset(self):
        # TODO: make sure we only return players that agreed to being seen
        # publicly
        tournament_id = self.kwargs['tournament_pk']
        return self.queryset.filter(
            tournament_id=tournament_id
        ).prefetch_related(
            Prefetch(
                'tournament_team_players',
                queryset=TournamentTeamPlayer.objects.order_by('player__first_name')
            )).all()
