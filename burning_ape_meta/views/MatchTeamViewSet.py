from django.db.models import Q
from rest_framework import (
    response,
    viewsets,
)
from rest_framework.decorators import action

from burning_ape_meta.access_policy import (
    MatchTeamAccessPolicy,
)
from burning_ape_meta.serializers import (
    MatchTeamSerializer,
)
from burning_ape_meta.models import (
    MatchTeam,
    Match,
    Person,
    TournamentTeamPlayer,
)


class MatchTeamViewSet(viewsets.ModelViewSet):
    permission_classes = [MatchTeamAccessPolicy]
    queryset = MatchTeam.objects.all()
    serializer_class = MatchTeamSerializer

    @action(methods=['get'], detail=True, permission_classes=[])
    def can_update(self, request, tournament_pk=None, pk=None):
        if request.user.is_staff:
            return response.Response(True)

        match_team = MatchTeam.objects.get(pk=pk)
        match = Match.objects.get(Q(right_team_id=match_team.id) | Q(left_team_id=match_team.id))

        if match.left_team.score is not None and match.right_team.score is not None:
            return response.Response(False)

        person = Person.objects.filter(account_id=request.user.id).first()
        if person is None:
            return response.Response(False)

        is_member = TournamentTeamPlayer.objects.filter(
            tournament_team_id=match_team.tournament_team_id, player_id=person.id
        ).exists()
        return response.Response(is_member)
