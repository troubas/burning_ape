from rest_framework import (
    viewsets,
)

from burning_ape_meta.access_policy import (
    ClubAccessPolicy,
)
from burning_ape_meta.serializers import (
    ClubSerializer,
)
from burning_ape_meta.models import (
    Club,
)


class ClubViewSet(viewsets.ModelViewSet):
    permission_classes = [ClubAccessPolicy]
    queryset = Club.objects.all()
    serializer_class = ClubSerializer
