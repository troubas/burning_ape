from rest_framework import (
    viewsets,
)

from burning_ape_meta.access_policy import (
    ProductAccessPolicy,
)
from burning_ape_meta.serializers import (
    ProductSerializer,
)
from burning_ape_meta.models import (
    Product,
)


class ProductViewSet(viewsets.ModelViewSet):
    permission_classes = [ProductAccessPolicy]
    queryset = Product.objects.order_by('product_name').all()
    serializer_class = ProductSerializer
