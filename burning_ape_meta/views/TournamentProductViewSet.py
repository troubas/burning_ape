from rest_framework import (
    viewsets,
)

from burning_ape_meta.access_policy import (
    TournamentProductAccessPolicy,
)
from burning_ape_meta.serializers import (
    TournamentProductSerializer,
)
from burning_ape_meta.models import (
    TournamentProduct,
)


class TournamentProductViewSet(viewsets.ModelViewSet):
    permission_classes = [TournamentProductAccessPolicy]
    queryset = TournamentProduct.objects.order_by('product__product_name').all()
    serializer_class = TournamentProductSerializer

    def get_queryset(self):
        return self.queryset.filter(tournament_id=self.kwargs['tournament_pk'])
