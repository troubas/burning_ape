from rest_framework import (
    viewsets,
)

from burning_ape_meta.access_policy import (
    PersonContactMechanismAccessPolicy,
)
from burning_ape_meta.serializers import (
    ContactMechanismSerializer,
)
from burning_ape_meta.models import (
    ContactMechanism,
)


class PersonContactMechanismViewSet(viewsets.ModelViewSet):
    permission_classes = [PersonContactMechanismAccessPolicy]
    queryset = ContactMechanism.objects.all()
    serializer_class = ContactMechanismSerializer
