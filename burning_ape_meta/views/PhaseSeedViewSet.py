from rest_framework import (
    viewsets,
)

from burning_ape_meta.access_policy import (
    TournamentPhaseSeedAccessPolicy,
)
from burning_ape_meta.serializers import (
    PhaseSeedSerializer,
)
from burning_ape_meta.models import (
    PhaseSeed,
)


class PhaseSeedViewSet(viewsets.ModelViewSet):
    permission_classes = [TournamentPhaseSeedAccessPolicy]
    serializer_class = PhaseSeedSerializer
    queryset = PhaseSeed.objects.all()
