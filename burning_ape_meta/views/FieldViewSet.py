from rest_framework import (
    viewsets,
)

from burning_ape_meta.access_policy import (
    FieldAccessPolicy,
)
from burning_ape_meta.serializers import (
    FieldSerializer,
)
from burning_ape_meta.models import (
    Field,
)


class FieldViewSet(viewsets.ModelViewSet):
    permission_classes = [FieldAccessPolicy]
    queryset = Field.objects.all()
    serializer_class = FieldSerializer
