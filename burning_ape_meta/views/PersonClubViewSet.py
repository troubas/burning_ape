from rest_framework import (
    viewsets,
)

from burning_ape_meta.access_policy import (
    PersonClubAccessPolicy,
)
from burning_ape_meta.serializers import (
    PersonClubSerializer,
)
from burning_ape_meta.models import (
    PersonClub,
)


class PersonClubViewSet(viewsets.ModelViewSet):
    permission_classes = [PersonClubAccessPolicy]
    serializer_class = PersonClubSerializer
    queryset = PersonClub.objects.all()
