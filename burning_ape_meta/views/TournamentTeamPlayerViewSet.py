from rest_framework import (
    viewsets,
)

from burning_ape_meta.access_policy import (
    TournamentTeamPlayerAccessPolicy,
)
from burning_ape_meta.serializers import (
    TournamentTeamPlayerSerializer,
)
from burning_ape_meta.models import (
    TournamentTeamPlayer,
)


class TournamentTeamPlayerViewSet(viewsets.ModelViewSet):
    permission_classes = [TournamentTeamPlayerAccessPolicy]
    queryset = TournamentTeamPlayer.objects.order_by('player__first_name').all()
    serializer_class = TournamentTeamPlayerSerializer
