from django.db.models import Q

from rest_framework import (
    viewsets,
)

from burning_ape_meta.access_policy import (
    MatchAccessPolicy,
)
from burning_ape_meta.serializers import (
    MatchSerializer,
)
from burning_ape_meta.models import (
    Match,
    MatchTeamSeed,
    TournamentPhase,
)
from burning_ape_meta.utils import (
    CustomQueryParamsSchema,
)


class MatchViewSet(viewsets.ModelViewSet):
    permission_classes = [MatchAccessPolicy]
    queryset = Match.objects.all()
    serializer_class = MatchSerializer
    schema = CustomQueryParamsSchema(
        query_params=[
            {
                'name': 'phase_id',
                'type': 'number'},
            {
                'name': 'group_id',
                'type': 'number'
            }
        ],
    )

    def get_queryset(self):
        match_ids = None
        seeds = None
        phase_id = self.request.query_params.get('phase_id')
        group_id = self.request.query_params.get('group_id')
        team_id = self.request.query_params.get('team_id')

        if (phase_id):
            phase = TournamentPhase.objects.get(pk=phase_id)
            seeds = MatchTeamSeed.objects.all().select_related(
                'seed_from_ranking__phase'
            )
            if (phase.game_mode == TournamentPhase.GameMode.Knockout):
                seeds = seeds.filter(
                    Q(seed_from_ranking__phase__pk=phase_id) |
                    Q(seed_from_ranking_id__isnull=True)
                )
            else:
                seeds = seeds.filter(
                    Q(seed_from_ranking__phase__pk=phase_id)
                )
            match_ids = seeds.values_list('matchteam__left_team__pk', flat=True)
        elif (group_id):
            seeds = MatchTeamSeed.objects.all().select_related(
                'seed_from_ranking__group'
            ).filter(
                seed_from_ranking__group__pk=group_id
            )
            match_ids = seeds.values_list('matchteam__left_team__pk', flat=True)
        elif (team_id):
            self.queryset = Match.objects.select_related(
                    'left_team',
                    'right_team'
            ).filter(
                    Q(left_team__tournament_team_id=team_id) |
                    Q(right_team__tournament_team_id=team_id)
            )

        if match_ids:
            return self.queryset.filter(
                pk__in=match_ids,
            ).order_by(
                'field_time_slot__start_time', 'field_time_slot__field__name'
            )
        else:
            return self.queryset.order_by('field_time_slot__start_time')
