from rest_framework import (
    viewsets,
)

from burning_ape_meta.access_policy import (
    FieldTimeSlotAccessPolicy,
)
from burning_ape_meta.serializers import (
    FieldTimeSlotSerializer,
)
from burning_ape_meta.models import (
    FieldTimeSlot,
)


class FieldTimeSlotViewSet(viewsets.ModelViewSet):
    permission_classes = [FieldTimeSlotAccessPolicy]
    queryset = FieldTimeSlot.objects.all()
    serializer_class = FieldTimeSlotSerializer
