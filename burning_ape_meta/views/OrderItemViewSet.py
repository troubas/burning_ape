from rest_framework import (
    viewsets,
)

from burning_ape_meta.access_policy import (
    OrderItemAccessPolicy,
)
from burning_ape_meta.serializers import (
    OrderItemSerializer,
)
from burning_ape_meta.models import (
    OrderItem,
)


class OrderItemViewSet(viewsets.ModelViewSet):
    permission_classes = [OrderItemAccessPolicy]
    queryset = OrderItem.objects.all()
    serializer_class = OrderItemSerializer

    def get_queryset(self):
        order_id = self.kwargs['order_pk']
        if (order_id):
            return self.queryset.filter(order_id=order_id)
        else:
            # TODO better exception
            raise Exception()
