from rest_framework import (
    viewsets,
)

from burning_ape_meta.access_policy import (
    ZipCodeAccessPolicy,
)
from burning_ape_meta.serializers import (
    ZipCodeSerializer,
)
from burning_ape_meta.models import (
    ZipCode,
)


class ZipCodeViewSet(viewsets.ModelViewSet):
    permission_classes = [ZipCodeAccessPolicy]
    queryset = ZipCode.objects.all()
    serializer_class = ZipCodeSerializer
