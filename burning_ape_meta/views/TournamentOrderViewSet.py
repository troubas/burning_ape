from django.db.models import F, Sum, QuerySet, Value
from django.db.models.functions import Concat

from rest_framework import (
    response,
    viewsets,
)
from rest_framework.decorators import action

from burning_ape_meta.access_policy import (
    TournamentOrderAccessPolicy,
    TournamentOrderStatsAccessPolicy,
)
from burning_ape_meta.models import (
    Order,
)
from burning_ape_meta.serializers import (
    OrderSerializer,
    OrderStatsSerializer,
)


class TournamentOrderViewSet(viewsets.ModelViewSet):
    permission_classes = [TournamentOrderAccessPolicy]
    serializer_class = OrderSerializer
    queryset: QuerySet[Order] = Order.objects.all().order_by('-order_datetime')

    def list(self, request, tournament_pk):
        queryset = self.queryset.filter(tournament_id=tournament_pk)[:100]
        serializer = OrderSerializer(queryset, many=True)
        return response.Response(serializer.data)

    @action(
        methods=['get'],
        detail=False,
        permission_classes=[TournamentOrderStatsAccessPolicy]
    )
    def stats(self, request, tournament_pk=None, pk=None):
        tournament_id = self.kwargs['tournament_pk']

        if (tournament_id):
            queryset = self.queryset.filter(
                tournament_id=tournament_id
            ).values(
                'person__pk',
                'person__first_name',
                'person__last_name'
            ).annotate(
                person_pk=F('person__pk'),
                name=Concat(
                    F('person__first_name'),
                    Value(' '),
                    F('person__last_name'),
                ),
                price=Sum(F('order_items__price') * F('order_items__quantity'))
            ).order_by('-price')

            serializer = OrderStatsSerializer(queryset, many=True)
            return response.Response(serializer.data)
        else:
            # todo: invalid argument exception?
            raise Exception()
