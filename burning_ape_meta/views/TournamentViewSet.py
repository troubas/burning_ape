from rest_framework import (
    viewsets,
)

from burning_ape_meta.access_policy import (
    TournamentAccessPolicy,
)
from burning_ape_meta.serializers import (
    TournamentSerializer,
)
from burning_ape_meta.models import (
    Tournament,
)


class TournamentViewSet(viewsets.ModelViewSet):
    permission_classes = [TournamentAccessPolicy]
    queryset = Tournament.objects.all()
    serializer_class = TournamentSerializer
