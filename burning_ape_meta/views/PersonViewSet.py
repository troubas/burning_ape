from rest_framework import (
    viewsets,
)

from burning_ape_meta.access_policy import (
    PersonAccessPolicy,
)
from burning_ape_meta.serializers import (
    PersonSerializer,
)
from burning_ape_meta.models import (
    Person,
)


class PersonViewSet(viewsets.ModelViewSet):
    permission_classes = [PersonAccessPolicy]
    serializer_class = PersonSerializer
    queryset = Person.objects.all()

    @property
    def access_policy(self):
        return self.permission_classes[0]

    def get_queryset(self):
        return self.access_policy.scope_queryset(
                self.request, self.queryset
                )
