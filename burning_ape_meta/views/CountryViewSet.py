from rest_framework import (
    viewsets,
)

from burning_ape_meta.access_policy import (
    CountryAccessPolicy,
)
from burning_ape_meta.serializers import (
    CountrySerializer,
)
from burning_ape_meta.models import (
    Country,
)


class CountryViewSet(viewsets.ModelViewSet):
    permission_classes = [CountryAccessPolicy]
    queryset = Country.objects.all()
    serializer_class = CountrySerializer
