from rest_framework import (
    viewsets,
)

from burning_ape_meta.access_policy import (
    UserAccessPolicy,
)
from burning_ape_meta.serializers import (
    UserSerializer,
)
from django.contrib.auth.models import (
    User,
)


class UserViewSet(viewsets.ModelViewSet):
    permission_classes = [UserAccessPolicy]
    queryset = User.objects.all()
    serializer_class = UserSerializer
