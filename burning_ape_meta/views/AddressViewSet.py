from rest_framework import (
    viewsets,
)

from burning_ape_meta.access_policy import (
    AddressAccessPolicy,
)
from burning_ape_meta.serializers import (
    AddressSerializer,
)
from burning_ape_meta.models import (
    Address,
)


class AddressViewSet(viewsets.ModelViewSet):
    permission_classes = [AddressAccessPolicy]
    queryset = Address.objects.all()
    serializer_class = AddressSerializer
