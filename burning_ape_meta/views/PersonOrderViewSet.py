from rest_framework import (
    viewsets,
)

from burning_ape_meta.access_policy import (
    PersonOrderAccessPolicy,
)
from burning_ape_meta.models import (
    Order,
)
from burning_ape_meta.serializers import (
    OrderSerializer,
)
from burning_ape_meta.utils import (
    CustomQueryParamsSchema,
)


class PersonOrderViewSet(viewsets.ModelViewSet):
    permission_classes = [PersonOrderAccessPolicy]
    serializer_class = OrderSerializer
    queryset = Order.objects.order_by('-order_datetime').all()
    schema = CustomQueryParamsSchema(
        operation_id='PersorOrder'
    )

    def get_queryset(self):
        person_id = self.kwargs['person_pk']
        if (person_id):
            return self.queryset.filter(person_id=person_id)
        else:
            # todo: invalid argument exception?
            raise Exception()
