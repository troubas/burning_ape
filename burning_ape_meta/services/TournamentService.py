"""
The TournamentService helps planning a Tournament. It takes care of generating
groups, brackets and matches as required by a given configuration.
"""
from django.db import transaction
from enum import Enum
from math import (
    ceil,
    floor,
    log2,
    pow,
)
from typing import List
from random import shuffle

from burning_ape_meta.models import (
    PhaseSeed,
    Tournament,
    TournamentGroup,
    TournamentPhase,
    Match,
    MatchTeam,
    MatchTeamSeed,
)


class PhaseType(Enum):
    GROUP = 'Group'
    ELIMINATION = 'Elimination'


class PhaseConfig:
    """
    Configuration options for a phase.
    """
    # TODO: fix possible values for type (either group or elimination)
    # later on we might add gauntlet mode etc.
    phase_type: PhaseType
    # TODO: fix possible values for mode
    # group: round-robin, ?
    # elimination: single-elimination, double-elimination
    mode: TournamentPhase.GameMode
    name: str
    size: int
    # TODO data validation
    # group: number of groups
    # elimination: number of brackets
    group_count: int
    number: int

    def __init__(
        self,
        phase_type: PhaseType,
        mode: TournamentPhase.GameMode,
        name: str,
        size: int,
        group_count: int,
        number: int,
    ):
        self.phase_type = phase_type
        self.mode = mode
        self.name = name
        self.size = size
        self.group_count = group_count
        self.number = number


class TournamentConfig:
    """
    Configuration to generate a tournament
    """
    name: str
    phase_configs: list[PhaseConfig]
    division: Tournament.Division
    date: str
    owner: int
    location: int

    def __init__(
        self,
        name: str,
        phase_configs: list[PhaseConfig],
        division: Tournament.Division,
        date: str,
        owner: int,
        location: int,
    ):
        self.name = name
        self.phase_configs = phase_configs
        self.division = division
        self.date = date
        self.owner = owner
        self.location = location


def generate_round_robin_matches(seeds: List[PhaseSeed]):
    group_ids = set([seed.group_id for seed in seeds])

    # loop over groups
    for group_id in group_ids:
        group_seeds = [seed for seed in seeds if seed.group_id == group_id]
        group_match_seeds = [
            (left, right)
            for left in group_seeds
            for right in group_seeds
        ]
        match_team_seeds: List[MatchTeamSeed] = []
        match_teams: List[MatchTeam] = []
        match_count: int = Match.objects.count()
        matches: List[Match] = []
        for (left, right) in [
                x for x in set(
                    map(frozenset[PhaseSeed], group_match_seeds)
                ) if len(x) == 2
        ]:
            left_seed = MatchTeamSeed(
                seed_from_ranking=left,
            )
            right_seed = MatchTeamSeed(
                seed_from_ranking=right,
            )
            left_team = MatchTeam(
                seed=left_seed,
            )
            right_team = MatchTeam(
                seed=right_seed,
            )
            match = Match(
                tournament_id=left.phase.tournament_id,
                name=str(len(matches) + match_count),
                left_team=left_team,
                right_team=right_team,
                allow_draw=True,
            )

            match_team_seeds.append(left_seed)
            match_team_seeds.append(right_seed)
            match_teams.append(left_team)
            match_teams.append(right_team)
            matches.append(match)

        MatchTeamSeed.objects.bulk_create(match_team_seeds)
        MatchTeam.objects.bulk_create(match_teams)
        Match.objects.bulk_create(matches)


def generate_group_phase(
    phase: TournamentPhase,
    group_count: int,
):
    """
    Generate groups and matches of group phase.
    """
    groups = [
        TournamentGroup(
            name="Group " + str(i),
            short_name="G" + str(i),
            phase=phase,
        ) for i in range(1, group_count + 1)
    ]
    TournamentGroup.objects.bulk_create(groups)

    ranks = range(1, phase.size + 1)
    phase_seed = [
        PhaseSeed(
            rank=ranks[i],
            phase=phase,
            # add every n-th rank to a group.
            group=groups[ranks[i] % group_count],
        ) for i in range(0, phase.size)
    ]
    PhaseSeed.objects.bulk_create(phase_seed)

    match phase.game_mode:
        case TournamentPhase.GameMode.RoundRobin:
            generate_round_robin_matches(phase_seed)
        case TournamentPhase.GameMode.SwissDraw:
            raise Exception('Not implemented (yet)')
        case TournamentPhase.GameMode.Knockout:
            raise Exception('Knockout not supported in group mode')


def generate_elimination_phase_round(
    round_nr: int,
    round_in: List[Match | PhaseSeed],
    games_in_round: int,
    phase: TournamentPhase,
) -> List[Match]:
    matches: List[Match] = []
    matchteamseeds: List[MatchTeamSeed] = []
    matchteams: List[MatchTeam] = []
    match_count: int = Match.objects.count()

    for j in range(0, games_in_round):
        right_match_seed = None
        right_rank_seed = None
        left_match_seed = None
        left_rank_seed = None
        if type(round_in[j]) == PhaseSeed:
            left_rank_seed = round_in[j]
        if type(round_in[j]) == Match:
            left_match_seed = round_in[j]

        if type(round_in[-j - 1]) == PhaseSeed:
            right_rank_seed = round_in[-j - 1]

        if type(round_in[-j - 1]) == Match:
            right_match_seed = round_in[-j - 1]

        # match the teams so that the highest rank plays against lowest rank
        left_seed = MatchTeamSeed(
            seed_from_ranking=left_rank_seed,
            seed_from_match=left_match_seed,
            seed_from_winner=True,
        )
        right_seed = MatchTeamSeed(
            seed_from_ranking=right_rank_seed,
            seed_from_match=right_match_seed,
            seed_from_winner=True,
        )
        matchteamseeds.append(left_seed)
        matchteamseeds.append(right_seed)

        left_team = MatchTeam(
            seed=left_seed,
        )
        right_team = MatchTeam(
            seed=right_seed,
        )
        matchteams.append(left_team)
        matchteams.append(right_team)

        match_count = match_count + 1
        matches.append(
            Match(
                left_team=left_team,
                right_team=right_team,
                tournament=phase.tournament,
                name=match_count,
                allow_draw=False,
            ))

    MatchTeamSeed.objects.bulk_create(matchteamseeds)
    MatchTeam.objects.bulk_create(matchteams)
    Match.objects.bulk_create(matches)
    return matches


def generate_elimination_phase(phase: TournamentPhase):
    rounds = ceil(log2(phase.size))
    # round_names = [f"Round {i + 1}" for i in range(0, rounds)]
    size_is_power_of_2: bool = floor(log2(phase.size)) == log2(phase.size)
    match_count: int = Match.objects.count()

    matches: List[Match] = []
    matchteamseeds: List[MatchTeamSeed] = []
    matchteams: List[MatchTeam] = []
    round_in: List[Match | PhaseSeed] = []

    ranking = [
        PhaseSeed(
            rank=i + 1,
            phase=phase,
        ) for i in range(0, phase.size)
    ]
    PhaseSeed.objects.bulk_create(ranking)

    if (not size_is_power_of_2):
        games_in_first_round: int = int(phase.size - pow(2, rounds - 1))
        remove_first_n_teams = int(phase.size - 2 * games_in_first_round)
        round_in = [ranking[i] for i in range(remove_first_n_teams, len(ranking))]

        for i in range(0, games_in_first_round):
            # match the teams so that the highest rank plays against lowest rank
            left_seed = MatchTeamSeed(
                seed_from_ranking=round_in[i],
                placeholder="#{}".format(i + remove_first_n_teams),
            )
            right_seed = MatchTeamSeed(
                seed_from_ranking=round_in[-(1 + i)],
                placeholder="#{}".format(len(ranking) - 1 - i - remove_first_n_teams),
            )
            matchteamseeds.append(left_seed)
            matchteamseeds.append(right_seed)

            left_team = MatchTeam(
                seed=left_seed,
            )
            right_team = MatchTeam(
                seed=right_seed,
            )
            matchteams.append(left_team)
            matchteams.append(right_team)

            match_count = match_count + 1
            matches.append(
                Match(
                    tournament=phase.tournament,
                    name=match_count,
                    left_team=left_team,
                    right_team=right_team,
                    allow_draw=False,
                ))

    MatchTeamSeed.objects.bulk_create(matchteamseeds)
    MatchTeam.objects.bulk_create(matchteams)
    Match.objects.bulk_create(matches)

    round_in = [ranking[i] for i in range(0, remove_first_n_teams)]
    for i in range(0, games_in_first_round):
        round_in.append(matches[i])

    for i in range(1, rounds):
        games_in_round = int(pow(2, rounds - i - 1))
        round_in = generate_elimination_phase_round(
            round_nr=i,
            round_in=round_in,
            games_in_round=games_in_round,
            phase=phase,
        )


def generate_tournament_phase(
    tournament_id: int,
    config: PhaseConfig,
):
    """
    """
    with transaction.atomic():
        tournament = Tournament.objects.get(id=tournament_id)
        phase = TournamentPhase.objects.create(
            name=config.name,
            game_mode=config.mode,
            tournament=tournament,
            size=config.size,
            phase_number=config.number,
        )
        # TODO: make phase_type known to phase? / use Enum
        match config.phase_type:
            case PhaseType.GROUP:
                generate_group_phase(phase, config.group_count)
            case PhaseType.ELIMINATION:
                generate_elimination_phase(phase)


def generate_tournament(conf: TournamentConfig):
    t = Tournament.objects.create(
        name=conf.name,
        game_mode=conf.division,
        date=conf.date,
        owner_id=conf.owner,
        location_id=conf.location,
    )
    for phase in conf.phase_configs:
        generate_tournament_phase(t.pk, phase)

    return t


def random_seeding(tournament: Tournament):
    phase: TournamentPhase = tournament.phases.order_by('phase_number').first()
    seeds = phase.phaseseed.order_by('rank')
    teams = list(tournament.teams.all())
    shuffle(teams)

    if len(teams) != seeds.count():
        raise Exception("team count and seed count don't match")

    idx = 0
    with transaction.atomic():
        for team in teams:
            seed = PhaseSeed.objects.get(pk=seeds[idx].pk)
            seed.team = team
            seed.save()

            for mts in seed.matchteamseed.all():
                matchteam = MatchTeam.objects.get(pk=mts.matchteam.pk)
                matchteam.tournament_team = team
                matchteam.save()

            idx += 1
