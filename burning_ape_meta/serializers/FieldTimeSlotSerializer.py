from rest_framework import (
    serializers,
)
from burning_ape_meta.models import (
    Field,
    FieldTimeSlot
)


class FieldTimeSlotSerializer(serializers.HyperlinkedModelSerializer):
    field: serializers.HyperlinkedRelatedField = serializers.HyperlinkedRelatedField(
        queryset=Field.objects.all(),
        view_name='field-detail',
    )

    class Meta:
        model = FieldTimeSlot
        fields = (
            'id',
            'url',
            'field',
            'start_time',
            'end_time',
        )
