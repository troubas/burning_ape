from rest_framework import (
    serializers,
)

from burning_ape_meta.models import (
    Person,
)


class PlayerSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Person
        fields = [
            'id',
            'first_name',
            'last_name',
        ]
