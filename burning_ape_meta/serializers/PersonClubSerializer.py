from rest_framework import (
    serializers,
)

from burning_ape_meta.models import (
    Club,
    Person,
    PersonClub,
)


class PersonClubSerializer(serializers.HyperlinkedModelSerializer):
    club: serializers.HyperlinkedRelatedField = serializers.HyperlinkedRelatedField(
        queryset=Club.objects.all(),
        view_name='club-detail',
    )
    person: serializers.HyperlinkedRelatedField = serializers.HyperlinkedRelatedField(
        queryset=Person.objects.all(),
        view_name='person-detail',
    )

    class Meta:
        model = PersonClub
        fields = [
            'id',
            'url',
            'club',
            'person',
            'date_from',
            'date_until',
            'player_number',
        ]
