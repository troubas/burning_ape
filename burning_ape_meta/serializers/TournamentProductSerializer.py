from rest_framework import (
    serializers,
)

from burning_ape_meta.models import (
    TournamentProduct,
)


class TournamentProductSerializer(serializers.HyperlinkedModelSerializer):
    price = serializers.FloatField(required=False)

    class Meta:
        model = TournamentProduct
        fields = (
            'id',
            'tournament_id',
            'category',
            'product_name',
            'product_id',
            'price'
        )
