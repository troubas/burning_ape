from rest_framework import (
    serializers,
)

from burning_ape_meta.models import (
    Product,
)


class ProductSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Product
        fields = (
            'id',
            'url',
            'category',
            'product_name',
            'price'
        )
