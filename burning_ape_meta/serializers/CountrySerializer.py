from rest_framework import (
    serializers,
)
from burning_ape_meta.models import (
    Country
)


class CountrySerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Country
        fields = (
            'id',
            'url',
            'country',
        )
