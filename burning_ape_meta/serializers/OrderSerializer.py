from django.db import transaction

from rest_framework import (
    serializers,
)

from .OrderItemSerializer import OrderItemSerializer
from burning_ape_meta.models import (
    Order,
    OrderItem,
)


class OrderSerializer(serializers.HyperlinkedModelSerializer):
    order_items = OrderItemSerializer(many=True)
    person_id = serializers.IntegerField()
    person_name = serializers.CharField(required=False)
    total_price = serializers.FloatField(read_only=True)

    class Meta:
        model = Order
        fields = [
            'id',
            'person_id',
            'person_name',
            'order_items',
            'total_price',
        ]

    def create(self, validated_data):
        order_items = validated_data.pop('order_items')

        validated_data['tournament_id'] = (
            self.context['request'].parser_context['kwargs']['tournament_pk']
        )
        with transaction.atomic():
            order = Order.objects.create(**validated_data)
            for order_item in order_items:
                OrderItem.objects.create(order=order, **order_item)
            return order
