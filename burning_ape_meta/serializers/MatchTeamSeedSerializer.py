from rest_framework import (
    serializers,
)
from burning_ape_meta.models import (
    MatchTeamSeed
)


class MatchTeamSeedSerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = MatchTeamSeed
        fields = [
            'id',
            'seed_from_match_id',
            'seed_from_ranking_id',
            'placeholder',
        ]
