from rest_framework import (
    serializers,
)

from .MatchTeamSerializer import MatchTeamSerializer
from burning_ape_meta.models import (
    Match,
    Tournament,
)


class MatchSerializer(serializers.HyperlinkedModelSerializer):
    left_team: MatchTeamSerializer = MatchTeamSerializer(
        many=False,
    )
    right_team: MatchTeamSerializer = MatchTeamSerializer(
        many=False,
    )
    tournament: serializers.HyperlinkedRelatedField = serializers.HyperlinkedRelatedField(
        queryset=Tournament.objects.all(),
        view_name='tournament-detail',
    )
    match_start = serializers.DateTimeField(read_only=True)
    match_end = serializers.DateTimeField(read_only=True)

    class Meta:
        model = Match
        fields = [
            'id',
            'left_team',
            'right_team',
            'tournament',
            'tournament_id',
            'allow_draw',
            'name',
            'played',
            'match_start',
            'match_end',
            'field_name',
        ]
