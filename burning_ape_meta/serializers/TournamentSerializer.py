from rest_framework import (
    serializers,
)
from rest_framework_nested.relations import NestedHyperlinkedRelatedField

from .AddressSerializer import AddressSerializer

from burning_ape_meta.models import (
    Tournament,
)


class TournamentSerializer(serializers.HyperlinkedModelSerializer):
    location = AddressSerializer()
    teams: NestedHyperlinkedRelatedField = NestedHyperlinkedRelatedField(
        view_name='tournamentteam-detail',
        parent_lookup_kwargs={'tournament_pk': 'tournament__pk'},
        many=True,
        read_only=True,
    )

    class Meta:
        model = Tournament
        fields = (
            'id',
            'url',
            'date',
            'game_mode',
            'location',
            'name',
            'teams',
        )
