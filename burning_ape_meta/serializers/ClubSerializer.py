from rest_framework import (
    serializers,
)
from burning_ape_meta.models import (
    Club
)


class ClubSerializer(serializers.ModelSerializer):
    # TODO: filter players and retrieve only the ones that agreed to be
    # publicly found.
    class Meta:
        model = Club
        fields = '__all__'
