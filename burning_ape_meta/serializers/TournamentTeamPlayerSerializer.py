from django.db import transaction

from rest_framework import (
    serializers,
)

from .PlayerSerializer import PlayerSerializer

from burning_ape_meta.models import (
    Person,
    TournamentTeam,
    TournamentTeamPlayer,
)


class TournamentTeamPlayerSerializer(serializers.HyperlinkedModelSerializer):
    player = PlayerSerializer()
    tournament_team_id = serializers.IntegerField()
    player_id = serializers.IntegerField(required=False)

    class Meta:
        model = TournamentTeamPlayer
        fields = (
            'player_id',
            'player',
            'tournament_team_id',
            'public_name'
        )

    def create(self, validated_data):
        with transaction.atomic():
            player = Person.objects.create(
                first_name=validated_data['player']['first_name'],
                last_name=validated_data['player']['last_name'])
            team = TournamentTeam.objects.get(id=validated_data['tournament_team_id'])
            ttp = TournamentTeamPlayer.objects.create(tournament_team=team, player=player)
            return ttp
