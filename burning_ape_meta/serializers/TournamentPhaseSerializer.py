from rest_framework import (
    serializers,
)
from rest_framework_nested.relations import NestedHyperlinkedRelatedField

from .TournamentGroupSerializer import TournamentGroupSerializer

from burning_ape_meta.models import (
    TournamentPhase,
)


class TournamentPhaseSerializer(serializers.HyperlinkedModelSerializer):
    url: NestedHyperlinkedRelatedField = NestedHyperlinkedRelatedField(
        view_name='tournamentphase-detail',
        parent_lookup_kwargs={'tournament_pk': 'tournament__pk'},
        allow_null=True,
        read_only=True,
    )

    groups = TournamentGroupSerializer(many=True)

    class Meta:
        model = TournamentPhase
        fields = (
            'id',
            'url',
            'tournament',
            'game_mode',
            'name',
            'phase_number',
            'size',
            'groups'
        )
