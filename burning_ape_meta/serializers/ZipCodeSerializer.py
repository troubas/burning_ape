from rest_framework import (
    serializers,
)

from burning_ape_meta.models import (
    ZipCode,
)


class ZipCodeSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = ZipCode
        fields = (
            'id',
            'url',
            'country',
            'country_name',
            'zip_code',
            'locality',
        )
