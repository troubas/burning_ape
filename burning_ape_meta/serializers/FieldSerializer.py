from rest_framework import (
    serializers,
)
from burning_ape_meta.models import (
    Address,
    Field
)


class FieldSerializer(serializers.HyperlinkedModelSerializer):
    location: serializers.HyperlinkedRelatedField = serializers.HyperlinkedRelatedField(
        queryset=Address.objects.all(),
        view_name='address-detail',
    )

    class Meta:
        model = Field
        fields = (
            'id',
            'url',
            'location',
            'name',
            'length',
            'width',
        )
