from rest_framework import (
    serializers,
)
from rest_framework_nested.relations import NestedHyperlinkedRelatedField

from burning_ape_meta.models import (
    ContactMechanism
)


class ContactMechanismSerializer(serializers.HyperlinkedModelSerializer):
    url: NestedHyperlinkedRelatedField = NestedHyperlinkedRelatedField(
        view_name='contact_mechanism-detail',
        parent_lookup_kwargs={'tournament_pk': 'tournament__pk'},
        allow_null=True,
        read_only=True,
    )

    class Meta:
        model = ContactMechanism
        fields = (
            'id',
            'url',
            'contact_type',
            'email',
            'phone_number',
            'address',
            'validated',
        )
