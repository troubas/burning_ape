from rest_framework import (
    serializers,
)
from rest_framework_nested.relations import NestedHyperlinkedRelatedField

from burning_ape_meta.models import (
    PhaseSeed,
)


class PhaseSeedSerializer(serializers.HyperlinkedModelSerializer):
    group: NestedHyperlinkedRelatedField = NestedHyperlinkedRelatedField(
        view_name='tournamentgroup-detail',
        parent_lookup_kwargs={'tournament_pk': 'phase__tournament__pk'},
        allow_null=True,
        read_only=True,
    )
    phase: NestedHyperlinkedRelatedField = NestedHyperlinkedRelatedField(
        view_name='tournamentphase-detail',
        parent_lookup_kwargs={'tournament_pk': 'tournament__pk'},
        allow_null=True,
        read_only=True,
    )
    team: NestedHyperlinkedRelatedField = NestedHyperlinkedRelatedField(
        view_name='tournamentteam-detail',
        parent_lookup_kwargs={'tournament_pk': 'tournament__pk'},
        allow_null=True,
        read_only=True,
    )

    class Meta:
        model = PhaseSeed
        fields = [
            'id',
            'group',
            'phase',
            'team',
            'rank',
        ]
