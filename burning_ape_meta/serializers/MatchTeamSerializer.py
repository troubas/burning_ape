from rest_framework import (
    serializers,
)
from rest_framework.utils import model_meta

from burning_ape_meta.models import (
    MatchTeam,
    MatchTeamSeed
)
from .MatchTeamSeedSerializer import MatchTeamSeedSerializer


class MatchTeamSerializer(serializers.HyperlinkedModelSerializer):
    display: serializers.ReadOnlyField = serializers.ReadOnlyField(
        source='get_display_name'
    )
    seed: MatchTeamSeedSerializer = MatchTeamSeedSerializer(
        read_only=True,
    )

    class Meta:
        model = MatchTeam
        fields = [
            'id',
            # 'tournament_team',
            'attitude_and_self_control',
            'communication',
            'fair_mindedness',
            'fouls_and_body_contact',
            'placeholder',
            'rules_knowledge_and_use',
            'score',
            'seed',
            'display',
            'spirit_remarks',
            'tournament_team_id',
        ]

    def update(self, instance, validated_data):
        info = model_meta.get_field_info(instance)

        # Simply set each attribute on the instance, and then save it.
        # Note that unlike `.create()` we don't need to treat many-to-many
        # relationships as being a special case. During updates we already
        # have an instance pk for the relationships to be associated with.
        m2m_fields = []
        for attr, value in validated_data.items():
            if attr in info.relations and info.relations[attr].to_many:
                m2m_fields.append((attr, value))
            else:
                setattr(instance, attr, value)
        instance.save()

        # Note that many-to-many fields are set after updating instance.
        # Setting m2m fields triggers signals which could potentially change
        # updated instance and we do not want it to collide with .update()
        for attr, value in m2m_fields:
            field = getattr(instance, attr)
            field.set(value)

        # After both results are received of the played match we have to update
        # the MatchTeam seeded by that match
        match = None
        if hasattr(instance, 'left_team'):
            match = instance.left_team
        elif hasattr(instance, 'right_team'):
            match = instance.right_team

        if match:
            match.played = True
            match.save()

            # both would be None if any of the results are missing
            winner = match.winner
            loser = match.loser

            if winner and loser:
                seeds = MatchTeamSeed.objects.filter(seed_from_match_id=match.id)

                for seed in seeds:
                    matchteam = seed.matchteam
                    if seed.seed_from_winner:
                        matchteam.tournament_team_id = winner.tournament_team_id
                    else:
                        matchteam.tournament_team_id = loser.tournament_team_id
                    matchteam.save()

        return instance
