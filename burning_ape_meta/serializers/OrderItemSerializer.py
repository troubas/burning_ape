from rest_framework import (
    serializers,
)

from burning_ape_meta.models import (
    OrderItem
)


class OrderItemSerializer(serializers.HyperlinkedModelSerializer):
    product_id = serializers.IntegerField()

    class Meta:
        model = OrderItem
        fields = (
            'id',
            'order_id',
            'product_id',
            'product_name',
            'quantity',
            'price'
        )
