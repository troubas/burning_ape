
from rest_framework import (
    serializers,
)
from burning_ape_meta.models import (
    Address,
    ZipCode
)


class AddressSerializer(serializers.HyperlinkedModelSerializer):
    zip_code = ZipCode()

    class Meta:
        model = Address
        fields = (
            'id',
            'url',
            'line1',
            'line2',
            'line3',
            'line4',
            'zip_code'
        )
