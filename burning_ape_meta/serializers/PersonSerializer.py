from rest_framework import (
    serializers,
)
from rest_framework_nested.relations import NestedHyperlinkedRelatedField

from burning_ape_meta.models import (
    Person
)


class PersonSerializer(serializers.HyperlinkedModelSerializer):
    teams: NestedHyperlinkedRelatedField = NestedHyperlinkedRelatedField(
        view_name='tournamentteam-detail',
        parent_lookup_kwargs={'player_pk': 'player__pk'},
        many=True,
        read_only=True,
    )

    class Meta:
        model = Person
        fields = [
            'id',
            'url',
            'age',
            'birthday_validated',
            'first_name',
            'last_name',
            'full_name',
            'teams',
        ]
