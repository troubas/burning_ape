from rest_framework import (
    serializers,
)
from rest_framework_nested.relations import NestedHyperlinkedRelatedField

from burning_ape_meta.models import (
    TournamentGroup,
)


class TournamentGroupSerializer(serializers.HyperlinkedModelSerializer):
    phase: NestedHyperlinkedRelatedField = NestedHyperlinkedRelatedField(
        view_name='tournamentphase-detail',
        parent_lookup_kwargs={'tournament_pk': 'tournament__pk'},
        allow_null=True,
        read_only=True,
    )

    class Meta:
        model = TournamentGroup
        fields = (
            'id',
            'phase',
            'name',
            'short_name',
        )
