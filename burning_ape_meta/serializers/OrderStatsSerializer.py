from rest_framework import (
    serializers,
)


class OrderStatsSerializer(serializers.Serializer):
    person_pk = serializers.IntegerField()
    name = serializers.CharField(max_length=200)
    price = serializers.FloatField()
