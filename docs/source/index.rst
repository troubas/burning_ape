.. Burning Ape App documentation master file, created by
   sphinx-quickstart on Mon Aug 22 10:53:42 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Burning Ape App's documentation!
===========================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

Modules
-------
.. toctree::
   :maxdepth: 2
   :caption: Contents:

   api/burning_ape_meta.rst



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
