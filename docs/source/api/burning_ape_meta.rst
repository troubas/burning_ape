Metadata
========

The burning_ape_meta package provides shared data to be used by different
modules.

Models
------

.. automodule:: burning_ape_meta.models
   :members: Player, Team, Field, Tournament
   :undoc-members:
   :show-inheritance:
