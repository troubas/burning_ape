# Burning Ape
Ridicilous over-sized project for the fucking Burning Ape.

_Wait what?_

Uhm let me see. The burning ape is an [ultimate] tournament with an openair
flavor. We like to go crazy. [@scorillazbern]

So now we want to go crazy digitally. We're strifing for the following features:
- Team & Player Management
- Game schedule, Scoring, Spirit, Assists/Points tracking
- Cashless payment system
- Provide MiniGames with scoring in the app
- Storytelling (tell the story of mahadsch ma gully)
- Find your Ape

__Everything written below is purely research based. No experience behind
it. Do not trust me.__

## Contributing
You're encouraged to do this using all your preferred tools and environments.
Otherwhise the following hopefully helps you to get started.

### Prepare system
Install [python], [virtualenv] and [pip]. These are the minimal required
packages to set up a virtual environment and manage python dependencies in a
system independent way.

### Installing dependencies in virtual environment
``` sh
source bin/activate
pip -r requirements.txt
```

### Set-Up Postgresql
Make sure that you have a running [postgresql] instance somewhere. Then change
the connection string in [settings.py] to that instance.

> Tip: If you create a PostgreSQL role/user with the same name as your Linux
> username, it allows you to access the PostgreSQL database shell without
> having to specify a user to login (which makes it quite convenient)
[archlinux-unix-login]

Once you've set up the connection run the SQL Migrations.

``` sh
./manage.py migrate
```

_or if you prefer:_
``` sh
python manage.py migrate
```

Then init the data
``` sh
./manage.py init_data
```

After that we have to reset our primary key increment because we set them manually
``` sh
./manage.py sqlsequencereset burning_ape_meta | psql database_name
```

Last but not least, create your superuser account
``` sh
./manage.py createsuperuser
```


### Run development server
``` sh
./manage.py runserver
```

### Documentation
You're going to need [sphinx] to build the documentation. Python modules are
documentation using [sphinx.ext.autodoc] a feature that uses the python
[docstring]s.

``` sh
source bin/activate
pip install sphinx
```

To generate the current erd enter the following commands
``` sh
./manage.py graph_models burning_ape_meta -a > model_burning_ape_meta.dot
dot -Tpdf model_burning_ape_meta.dot -o  model_burning_ape_meta.pdf
```

## Guidelines
I'm using [flake8] for linting, python [docstring]s for documentation (with
some [sphinx] features sprinkled in for references).

You can install [flake8] inside your virtual environment. And then you probably
have to configure your editor to run flake either continuosly or on save.

``` sh
source bin/activate
pip install flake8
```


[python]: https://www.python.org/
[postgresql]: https://www.postgresql.org/
[virtualenv]: https://virtualenv.pypa.io/en/latest/
[pip]: https://pip.pypa.io/en/stable/
[archlinux-unix-login]: https://wiki.archlinux.org/title/PostgreSQL#Create_your_first_database/user
[settings.py]: /burning_ape/settings.py
[ultimate]: https://en.wikipedia.org/wiki/Ultimate_(sport)
[@scorillazbern]: https://www.instagram.com/scorillaz_bern/
[flake8]: https://flake8.pycqa.org/en/latest/
[docstring]: https://peps.python.org/pep-0257/
[sphinx]: https://www.sphinx-doc.org/en/master/
[sphinx.ext.autodoc]: https://www.sphinx-doc.org/en/master/usage/extensions/autodoc.html
