# frontend

This template should help get you started developing with Vue 3 in Vite.

## Recommended IDE Setup

[VIM](https://www.vim.org/)

## Dev-Guidelines

### Styling

We are using flowbites components for styling, have a look: https://flowbite-vue.com/

### Linting

We use huskey to lint the changes and execute prettier on a pre-commit hook. It gets configured while setting up the application. So, just enjoy the errors.

## Project Setup

```sh
yarn
```

### Compile and Hot-Reload for Development

```sh
yarn dev
```

### Run Unit Tests with [Vitest](https://vitest.dev/)

```sh
yarn test:unit
```

### Lint with [ESLint](https://eslint.org/)

```sh
yarn lint
```
