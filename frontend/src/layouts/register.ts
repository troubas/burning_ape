import DefaultLayout from "@/layouts/DefaultLayout.vue";
import { App } from "vue";
import AppLayout from "./AppLayout.vue";

/**
 * Register layouts in the app instance
 *
 * @param {App<Element>} app
 */
export function registerLayouts(app: App<Element>) {
  app.component("AppLayout", AppLayout);
  app.component("DefaultLayout", DefaultLayout);
}
