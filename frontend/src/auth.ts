export const isLoggedIn = () => {
  const api_key = sessionStorage.getItem("oauth_token");
  console.log(api_key ? "logged in!" : "not logged in!");
  console.log(api_key);
  return api_key;
};
