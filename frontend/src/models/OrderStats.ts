export type OrderStats = {
  readonly person_pk: number;
  readonly name: string;
  readonly price: number;
};
