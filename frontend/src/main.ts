import { createApp } from "vue";
import { createPinia } from "pinia";
import App from "./App.vue";
import router from "./router";
import "@/assets/base.css";

import "vuetify/styles";
import "@mdi/font/css/materialdesignicons.css";

import { createVuetify } from "vuetify";
import { md2 } from "vuetify/blueprints";
import { registerLayouts } from "./layouts/register";

const vuetify = createVuetify({
  blueprint: md2,
  theme: {
    defaultTheme: "light",
  },
});

const app = createApp(App);

// Pinia Store
const pinia = createPinia();

// Layouts
registerLayouts(app);

app.use(router);
app.use(vuetify);
app.use(pinia);

app.mount("#app");
