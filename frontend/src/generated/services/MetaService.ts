/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { Address } from "../models/Address";
import type { Club } from "../models/Club";
import type { Country } from "../models/Country";
import type { Field } from "../models/Field";
import type { Match } from "../models/Match";
import type { MatchTeam } from "../models/MatchTeam";
import type { Order } from "../models/Order";
import type { OrderItem } from "../models/OrderItem";
import type { Person } from "../models/Person";
import type { PersonClub } from "../models/PersonClub";
import type { PhaseSeed } from "../models/PhaseSeed";
import type { Product } from "../models/Product";
import type { Tournament } from "../models/Tournament";
import type { TournamentGroup } from "../models/TournamentGroup";
import type { TournamentPhase } from "../models/TournamentPhase";
import type { TournamentProduct } from "../models/TournamentProduct";
import type { TournamentTeam } from "../models/TournamentTeam";
import type { TournamentTeamPlayer } from "../models/TournamentTeamPlayer";
import type { User } from "../models/User";
import type { ZipCode } from "../models/ZipCode";
import type { OrderStats } from "../../models/OrderStats";

import type { CancelablePromise } from "../core/CancelablePromise";
import { OpenAPI } from "../core/OpenAPI";
import { request as __request } from "../core/request";
import { WithId } from "../../../global";

export class MetaService {
  /**
   * @returns Address
   * @throws ApiError
   */
  public static listAddress(): CancelablePromise<Array<Address>> {
    return __request(OpenAPI, {
      method: "GET",
      url: "/meta/adresses/",
    });
  }

  /**
   * @param requestBody
   * @returns Address
   * @throws ApiError
   */
  public static createAddress(
    requestBody?: Address
  ): CancelablePromise<Address> {
    return __request(OpenAPI, {
      method: "POST",
      url: "/meta/adresses/",
      body: requestBody,
      mediaType: "application/json",
    });
  }

  /**
   * @param id A unique integer value identifying this address.
   * @returns Address
   * @throws ApiError
   */
  public static retrieveAddress(id: string): CancelablePromise<Address> {
    return __request(OpenAPI, {
      method: "GET",
      url: "/meta/adresses/{id}/",
      path: {
        id: id,
      },
    });
  }

  /**
   * @param id A unique integer value identifying this address.
   * @param requestBody
   * @returns Address
   * @throws ApiError
   */
  public static updateAddress(
    id: string,
    requestBody?: Address
  ): CancelablePromise<Address> {
    return __request(OpenAPI, {
      method: "PUT",
      url: "/meta/adresses/{id}/",
      path: {
        id: id,
      },
      body: requestBody,
      mediaType: "application/json",
    });
  }

  /**
   * @param id A unique integer value identifying this address.
   * @param requestBody
   * @returns Address
   * @throws ApiError
   */
  public static partialUpdateAddress(
    id: string,
    requestBody?: Address
  ): CancelablePromise<Address> {
    return __request(OpenAPI, {
      method: "PATCH",
      url: "/meta/adresses/{id}/",
      path: {
        id: id,
      },
      body: requestBody,
      mediaType: "application/json",
    });
  }

  /**
   * @param id A unique integer value identifying this address.
   * @returns void
   * @throws ApiError
   */
  public static destroyAddress(id: string): CancelablePromise<void> {
    return __request(OpenAPI, {
      method: "DELETE",
      url: "/meta/adresses/{id}/",
      path: {
        id: id,
      },
    });
  }

  /**
   * @returns Club
   * @throws ApiError
   */
  public static listClubs(): CancelablePromise<Array<Club>> {
    return __request(OpenAPI, {
      method: "GET",
      url: "/meta/clubs/",
    });
  }

  /**
   * @param requestBody
   * @returns Club
   * @throws ApiError
   */
  public static createClub(requestBody?: Club): CancelablePromise<Club> {
    return __request(OpenAPI, {
      method: "POST",
      url: "/meta/clubs/",
      body: requestBody,
      mediaType: "application/json",
    });
  }

  /**
   * @param id A unique integer value identifying this club.
   * @returns Club
   * @throws ApiError
   */
  public static retrieveClub(id: string): CancelablePromise<Club> {
    return __request(OpenAPI, {
      method: "GET",
      url: "/meta/clubs/{id}/",
      path: {
        id: id,
      },
    });
  }

  /**
   * @param id A unique integer value identifying this club.
   * @param requestBody
   * @returns Club
   * @throws ApiError
   */
  public static updateClub(
    id: string,
    requestBody?: Club
  ): CancelablePromise<Club> {
    return __request(OpenAPI, {
      method: "PUT",
      url: "/meta/clubs/{id}/",
      path: {
        id: id,
      },
      body: requestBody,
      mediaType: "application/json",
    });
  }

  /**
   * @param id A unique integer value identifying this club.
   * @param requestBody
   * @returns Club
   * @throws ApiError
   */
  public static partialUpdateClub(
    id: string,
    requestBody?: Club
  ): CancelablePromise<Club> {
    return __request(OpenAPI, {
      method: "PATCH",
      url: "/meta/clubs/{id}/",
      path: {
        id: id,
      },
      body: requestBody,
      mediaType: "application/json",
    });
  }

  /**
   * @param id A unique integer value identifying this club.
   * @returns void
   * @throws ApiError
   */
  public static destroyClub(id: string): CancelablePromise<void> {
    return __request(OpenAPI, {
      method: "DELETE",
      url: "/meta/clubs/{id}/",
      path: {
        id: id,
      },
    });
  }

  /**
   * @returns Country
   * @throws ApiError
   */
  public static listCountrys(): CancelablePromise<Array<Country>> {
    return __request(OpenAPI, {
      method: "GET",
      url: "/meta/countries/",
    });
  }

  /**
   * @param requestBody
   * @returns Country
   * @throws ApiError
   */
  public static createCountry(
    requestBody?: Country
  ): CancelablePromise<Country> {
    return __request(OpenAPI, {
      method: "POST",
      url: "/meta/countries/",
      body: requestBody,
      mediaType: "application/json",
    });
  }

  /**
   * @param id A unique integer value identifying this country.
   * @returns Country
   * @throws ApiError
   */
  public static retrieveCountry(id: string): CancelablePromise<Country> {
    return __request(OpenAPI, {
      method: "GET",
      url: "/meta/countries/{id}/",
      path: {
        id: id,
      },
    });
  }

  /**
   * @param id A unique integer value identifying this country.
   * @param requestBody
   * @returns Country
   * @throws ApiError
   */
  public static updateCountry(
    id: string,
    requestBody?: Country
  ): CancelablePromise<Country> {
    return __request(OpenAPI, {
      method: "PUT",
      url: "/meta/countries/{id}/",
      path: {
        id: id,
      },
      body: requestBody,
      mediaType: "application/json",
    });
  }

  /**
   * @param id A unique integer value identifying this country.
   * @param requestBody
   * @returns Country
   * @throws ApiError
   */
  public static partialUpdateCountry(
    id: string,
    requestBody?: Country
  ): CancelablePromise<Country> {
    return __request(OpenAPI, {
      method: "PATCH",
      url: "/meta/countries/{id}/",
      path: {
        id: id,
      },
      body: requestBody,
      mediaType: "application/json",
    });
  }

  /**
   * @param id A unique integer value identifying this country.
   * @returns void
   * @throws ApiError
   */
  public static destroyCountry(id: string): CancelablePromise<void> {
    return __request(OpenAPI, {
      method: "DELETE",
      url: "/meta/countries/{id}/",
      path: {
        id: id,
      },
    });
  }

  /**
   * @returns Field
   * @throws ApiError
   */
  public static listFields(): CancelablePromise<Array<Field>> {
    return __request(OpenAPI, {
      method: "GET",
      url: "/meta/fields/",
    });
  }

  /**
   * @param requestBody
   * @returns Field
   * @throws ApiError
   */
  public static createField(requestBody?: Field): CancelablePromise<Field> {
    return __request(OpenAPI, {
      method: "POST",
      url: "/meta/fields/",
      body: requestBody,
      mediaType: "application/json",
    });
  }

  /**
   * @param id A unique integer value identifying this field.
   * @returns Field
   * @throws ApiError
   */
  public static retrieveField(id: string): CancelablePromise<Field> {
    return __request(OpenAPI, {
      method: "GET",
      url: "/meta/fields/{id}/",
      path: {
        id: id,
      },
    });
  }

  /**
   * @param id A unique integer value identifying this field.
   * @param requestBody
   * @returns Field
   * @throws ApiError
   */
  public static updateField(
    id: string,
    requestBody?: Field
  ): CancelablePromise<Field> {
    return __request(OpenAPI, {
      method: "PUT",
      url: "/meta/fields/{id}/",
      path: {
        id: id,
      },
      body: requestBody,
      mediaType: "application/json",
    });
  }

  /**
   * @param id A unique integer value identifying this field.
   * @param requestBody
   * @returns Field
   * @throws ApiError
   */
  public static partialUpdateField(
    id: string,
    requestBody?: Field
  ): CancelablePromise<Field> {
    return __request(OpenAPI, {
      method: "PATCH",
      url: "/meta/fields/{id}/",
      path: {
        id: id,
      },
      body: requestBody,
      mediaType: "application/json",
    });
  }

  /**
   * @param id A unique integer value identifying this field.
   * @returns void
   * @throws ApiError
   */
  public static destroyField(id: string): CancelablePromise<void> {
    return __request(OpenAPI, {
      method: "DELETE",
      url: "/meta/fields/{id}/",
      path: {
        id: id,
      },
    });
  }

  /**
   * A viewset for viewing and editing tournaments.
   * @returns Tournament
   * @throws ApiError
   */
  public static listTournaments(): CancelablePromise<Array<Tournament>> {
    return __request(OpenAPI, {
      method: "GET",
      url: "/meta/tournaments/",
    });
  }

  /**
   * A viewset for viewing and editing tournaments.
   * @param requestBody
   * @returns Tournament
   * @throws ApiError
   */
  public static createTournament(
    requestBody?: Tournament
  ): CancelablePromise<Tournament> {
    return __request(OpenAPI, {
      method: "POST",
      url: "/meta/tournaments/",
      body: requestBody,
      mediaType: "application/json",
    });
  }

  /**
   * A viewset for viewing and editing tournaments.
   * @param id A unique integer value identifying this tournament.
   * @returns Tournament
   * @throws ApiError
   */
  public static retrieveTournament(id: string): CancelablePromise<Tournament> {
    return __request(OpenAPI, {
      method: "GET",
      url: "/meta/tournaments/{id}/",
      path: {
        id: id,
      },
    });
  }

  /**
   * A viewset for viewing and editing tournaments.
   * @param id A unique integer value identifying this tournament.
   * @param requestBody
   * @returns Tournament
   * @throws ApiError
   */
  public static updateTournament(
    id: string,
    requestBody?: Tournament
  ): CancelablePromise<Tournament> {
    return __request(OpenAPI, {
      method: "PUT",
      url: "/meta/tournaments/{id}/",
      path: {
        id: id,
      },
      body: requestBody,
      mediaType: "application/json",
    });
  }

  /**
   * A viewset for viewing and editing tournaments.
   * @param id A unique integer value identifying this tournament.
   * @param requestBody
   * @returns Tournament
   * @throws ApiError
   */
  public static partialUpdateTournament(
    id: string,
    requestBody?: Tournament
  ): CancelablePromise<Tournament> {
    return __request(OpenAPI, {
      method: "PATCH",
      url: "/meta/tournaments/{id}/",
      path: {
        id: id,
      },
      body: requestBody,
      mediaType: "application/json",
    });
  }

  /**
   * A viewset for viewing and editing tournaments.
   * @param id A unique integer value identifying this tournament.
   * @returns void
   * @throws ApiError
   */
  public static destroyTournament(id: string): CancelablePromise<void> {
    return __request(OpenAPI, {
      method: "DELETE",
      url: "/meta/tournaments/{id}/",
      path: {
        id: id,
      },
    });
  }

  /**
   * @returns Person
   * @throws ApiError
   */
  public static listPersons(): CancelablePromise<Array<Person>> {
    return __request(OpenAPI, {
      method: "GET",
      url: "/meta/persons/",
    });
  }

  /**
   * @param requestBody
   * @returns Person
   * @throws ApiError
   */
  public static createPerson(requestBody?: Person): CancelablePromise<Person> {
    return __request(OpenAPI, {
      method: "POST",
      url: "/meta/persons/",
      body: requestBody,
      mediaType: "application/json",
    });
  }

  /**
   * @param id A unique integer value identifying this person.
   * @returns Person
   * @throws ApiError
   */
  public static retrievePerson(id: string): CancelablePromise<Person> {
    return __request(OpenAPI, {
      method: "GET",
      url: "/meta/persons/{id}/",
      path: {
        id: id,
      },
    });
  }

  /**
   * @param id A unique integer value identifying this person.
   * @param requestBody
   * @returns Person
   * @throws ApiError
   */
  public static updatePerson(
    id: string,
    requestBody?: Person
  ): CancelablePromise<Person> {
    return __request(OpenAPI, {
      method: "PUT",
      url: "/meta/persons/{id}/",
      path: {
        id: id,
      },
      body: requestBody,
      mediaType: "application/json",
    });
  }

  /**
   * @param id A unique integer value identifying this person.
   * @param requestBody
   * @returns Person
   * @throws ApiError
   */
  public static partialUpdatePerson(
    id: string,
    requestBody?: Person
  ): CancelablePromise<Person> {
    return __request(OpenAPI, {
      method: "PATCH",
      url: "/meta/persons/{id}/",
      path: {
        id: id,
      },
      body: requestBody,
      mediaType: "application/json",
    });
  }

  /**
   * @param id A unique integer value identifying this person.
   * @returns void
   * @throws ApiError
   */
  public static destroyPerson(id: string): CancelablePromise<void> {
    return __request(OpenAPI, {
      method: "DELETE",
      url: "/meta/persons/{id}/",
      path: {
        id: id,
      },
    });
  }

  /**
   * @returns Product
   * @throws ApiError
   */
  public static listProducts(): CancelablePromise<Array<Product>> {
    return __request(OpenAPI, {
      method: "GET",
      url: "/meta/products/",
    });
  }

  /**
   * @param requestBody
   * @returns Product
   * @throws ApiError
   */
  public static createProduct(
    requestBody?: Product
  ): CancelablePromise<Product> {
    return __request(OpenAPI, {
      method: "POST",
      url: "/meta/products/",
      body: requestBody,
      mediaType: "application/json",
    });
  }

  /**
   * @param id A unique integer value identifying this product.
   * @returns Product
   * @throws ApiError
   */
  public static retrieveProduct(id: string): CancelablePromise<Product> {
    return __request(OpenAPI, {
      method: "GET",
      url: "/meta/products/{id}/",
      path: {
        id: id,
      },
    });
  }

  /**
   * @param id A unique integer value identifying this product.
   * @param requestBody
   * @returns Product
   * @throws ApiError
   */
  public static updateProduct(
    id: string,
    requestBody?: Product
  ): CancelablePromise<Product> {
    return __request(OpenAPI, {
      method: "PUT",
      url: "/meta/products/{id}/",
      path: {
        id: id,
      },
      body: requestBody,
      mediaType: "application/json",
    });
  }

  /**
   * @param id A unique integer value identifying this product.
   * @param requestBody
   * @returns Product
   * @throws ApiError
   */
  public static partialUpdateProduct(
    id: string,
    requestBody?: Product
  ): CancelablePromise<Product> {
    return __request(OpenAPI, {
      method: "PATCH",
      url: "/meta/products/{id}/",
      path: {
        id: id,
      },
      body: requestBody,
      mediaType: "application/json",
    });
  }

  /**
   * @param id A unique integer value identifying this product.
   * @returns void
   * @throws ApiError
   */
  public static destroyProduct(id: string): CancelablePromise<void> {
    return __request(OpenAPI, {
      method: "DELETE",
      url: "/meta/products/{id}/",
      path: {
        id: id,
      },
    });
  }

  /**
   * @returns User
   * @throws ApiError
   */
  public static listUsers(): CancelablePromise<Array<User>> {
    return __request(OpenAPI, {
      method: "GET",
      url: "/meta/users/",
    });
  }

  /**
   * @param requestBody
   * @returns User
   * @throws ApiError
   */
  public static createUser(requestBody?: User): CancelablePromise<User> {
    return __request(OpenAPI, {
      method: "POST",
      url: "/meta/users/",
      body: requestBody,
      mediaType: "application/json",
    });
  }

  /**
   * @param id A unique integer value identifying this user.
   * @returns User
   * @throws ApiError
   */
  public static retrieveUser(id: string): CancelablePromise<User> {
    return __request(OpenAPI, {
      method: "GET",
      url: "/meta/users/{id}/",
      path: {
        id: id,
      },
    });
  }

  /**
   * @param id A unique integer value identifying this user.
   * @param requestBody
   * @returns User
   * @throws ApiError
   */
  public static updateUser(
    id: string,
    requestBody?: User
  ): CancelablePromise<User> {
    return __request(OpenAPI, {
      method: "PUT",
      url: "/meta/users/{id}/",
      path: {
        id: id,
      },
      body: requestBody,
      mediaType: "application/json",
    });
  }

  /**
   * @param id A unique integer value identifying this user.
   * @param requestBody
   * @returns User
   * @throws ApiError
   */
  public static partialUpdateUser(
    id: string,
    requestBody?: User
  ): CancelablePromise<User> {
    return __request(OpenAPI, {
      method: "PATCH",
      url: "/meta/users/{id}/",
      path: {
        id: id,
      },
      body: requestBody,
      mediaType: "application/json",
    });
  }

  /**
   * @param id A unique integer value identifying this user.
   * @returns void
   * @throws ApiError
   */
  public static destroyUser(id: string): CancelablePromise<void> {
    return __request(OpenAPI, {
      method: "DELETE",
      url: "/meta/users/{id}/",
      path: {
        id: id,
      },
    });
  }

  /**
   * @returns ZipCode
   * @throws ApiError
   */
  public static listZipCodes(): CancelablePromise<Array<ZipCode>> {
    return __request(OpenAPI, {
      method: "GET",
      url: "/meta/zip_codes/",
    });
  }

  /**
   * @param requestBody
   * @returns ZipCode
   * @throws ApiError
   */
  public static createZipCode(
    requestBody?: ZipCode
  ): CancelablePromise<ZipCode> {
    return __request(OpenAPI, {
      method: "POST",
      url: "/meta/zip_codes/",
      body: requestBody,
      mediaType: "application/json",
    });
  }

  /**
   * @param id A unique integer value identifying this zip code.
   * @returns ZipCode
   * @throws ApiError
   */
  public static retrieveZipCode(id: string): CancelablePromise<ZipCode> {
    return __request(OpenAPI, {
      method: "GET",
      url: "/meta/zip_codes/{id}/",
      path: {
        id: id,
      },
    });
  }

  /**
   * @param id A unique integer value identifying this zip code.
   * @param requestBody
   * @returns ZipCode
   * @throws ApiError
   */
  public static updateZipCode(
    id: string,
    requestBody?: ZipCode
  ): CancelablePromise<ZipCode> {
    return __request(OpenAPI, {
      method: "PUT",
      url: "/meta/zip_codes/{id}/",
      path: {
        id: id,
      },
      body: requestBody,
      mediaType: "application/json",
    });
  }

  /**
   * @param id A unique integer value identifying this zip code.
   * @param requestBody
   * @returns ZipCode
   * @throws ApiError
   */
  public static partialUpdateZipCode(
    id: string,
    requestBody?: ZipCode
  ): CancelablePromise<ZipCode> {
    return __request(OpenAPI, {
      method: "PATCH",
      url: "/meta/zip_codes/{id}/",
      path: {
        id: id,
      },
      body: requestBody,
      mediaType: "application/json",
    });
  }

  /**
   * @param id A unique integer value identifying this zip code.
   * @returns void
   * @throws ApiError
   */
  public static destroyZipCode(id: string): CancelablePromise<void> {
    return __request(OpenAPI, {
      method: "DELETE",
      url: "/meta/zip_codes/{id}/",
      path: {
        id: id,
      },
    });
  }

  /**
   * @param tournamentPk
   * @returns TournamentGroup
   * @throws ApiError
   */
  public static listTournamentGroups(
    tournamentPk: string
  ): CancelablePromise<Array<TournamentGroup>> {
    return __request(OpenAPI, {
      method: "GET",
      url: "/meta/tournaments/{tournament_pk}/groups/",
      path: {
        tournament_pk: tournamentPk,
      },
    });
  }

  /**
   * @param tournamentPk
   * @param requestBody
   * @returns TournamentGroup
   * @throws ApiError
   */
  public static createTournamentGroup(
    tournamentPk: string,
    requestBody?: TournamentGroup
  ): CancelablePromise<TournamentGroup> {
    return __request(OpenAPI, {
      method: "POST",
      url: "/meta/tournaments/{tournament_pk}/groups/",
      path: {
        tournament_pk: tournamentPk,
      },
      body: requestBody,
      mediaType: "application/json",
    });
  }

  /**
   * @param tournamentPk
   * @param id A unique integer value identifying this tournament group.
   * @returns TournamentGroup
   * @throws ApiError
   */
  public static retrieveTournamentGroup(
    tournamentPk: string,
    id: string
  ): CancelablePromise<TournamentGroup> {
    return __request(OpenAPI, {
      method: "GET",
      url: "/meta/tournaments/{tournament_pk}/groups/{id}/",
      path: {
        tournament_pk: tournamentPk,
        id: id,
      },
    });
  }

  /**
   * @param tournamentPk
   * @param id A unique integer value identifying this tournament group.
   * @param requestBody
   * @returns TournamentGroup
   * @throws ApiError
   */
  public static updateTournamentGroup(
    tournamentPk: string,
    id: string,
    requestBody?: TournamentGroup
  ): CancelablePromise<TournamentGroup> {
    return __request(OpenAPI, {
      method: "PUT",
      url: "/meta/tournaments/{tournament_pk}/groups/{id}/",
      path: {
        tournament_pk: tournamentPk,
        id: id,
      },
      body: requestBody,
      mediaType: "application/json",
    });
  }

  /**
   * @param tournamentPk
   * @param id A unique integer value identifying this tournament group.
   * @param requestBody
   * @returns TournamentGroup
   * @throws ApiError
   */
  public static partialUpdateTournamentGroup(
    tournamentPk: string,
    id: string,
    requestBody?: TournamentGroup
  ): CancelablePromise<TournamentGroup> {
    return __request(OpenAPI, {
      method: "PATCH",
      url: "/meta/tournaments/{tournament_pk}/groups/{id}/",
      path: {
        tournament_pk: tournamentPk,
        id: id,
      },
      body: requestBody,
      mediaType: "application/json",
    });
  }

  /**
   * @param tournamentPk
   * @param id A unique integer value identifying this tournament group.
   * @returns void
   * @throws ApiError
   */
  public static destroyTournamentGroup(
    tournamentPk: string,
    id: string
  ): CancelablePromise<void> {
    return __request(OpenAPI, {
      method: "DELETE",
      url: "/meta/tournaments/{tournament_pk}/groups/{id}/",
      path: {
        tournament_pk: tournamentPk,
        id: id,
      },
    });
  }

  /**
   * @param tournamentPk
   * @param phaseId
   * @param groupId
   * @param teamId
   * @returns Match
   * @throws ApiError
   */
  public static listMatchs(
    tournamentPk: string,
    phaseId?: number,
    groupId?: number,
    teamId?: number
  ): CancelablePromise<Array<Match>> {
    return __request(OpenAPI, {
      method: "GET",
      url: "/meta/tournaments/{tournament_pk}/matches/",
      path: {
        tournament_pk: tournamentPk,
      },
      query: {
        phase_id: phaseId,
        group_id: groupId,
        team_id: teamId,
      },
    });
  }

  /**
   * @param tournamentPk
   * @param requestBody
   * @returns Match
   * @throws ApiError
   */
  public static createMatch(
    tournamentPk: string,
    requestBody?: Match
  ): CancelablePromise<Match> {
    return __request(OpenAPI, {
      method: "POST",
      url: "/meta/tournaments/{tournament_pk}/matches/",
      path: {
        tournament_pk: tournamentPk,
      },
      body: requestBody,
      mediaType: "application/json",
    });
  }

  /**
   * @param tournamentPk
   * @param id A unique integer value identifying this match.
   * @returns Match
   * @throws ApiError
   */
  public static retrieveMatch(
    tournamentPk: string,
    id: string
  ): CancelablePromise<Match> {
    return __request(OpenAPI, {
      method: "GET",
      url: "/meta/tournaments/{tournament_pk}/matches/{id}/",
      path: {
        tournament_pk: tournamentPk,
        id: id,
      },
    });
  }

  /**
   * @param tournamentPk
   * @param id A unique integer value identifying this match.
   * @param requestBody
   * @returns Match
   * @throws ApiError
   */
  public static updateMatch(
    tournamentPk: string,
    id: string,
    requestBody?: Match
  ): CancelablePromise<Match> {
    return __request(OpenAPI, {
      method: "PUT",
      url: "/meta/tournaments/{tournament_pk}/matches/{id}/",
      path: {
        tournament_pk: tournamentPk,
        id: id,
      },
      body: requestBody,
      mediaType: "application/json",
    });
  }

  /**
   * @param tournamentPk
   * @param id A unique integer value identifying this match.
   * @param requestBody
   * @returns Match
   * @throws ApiError
   */
  public static partialUpdateMatch(
    tournamentPk: string,
    id: string,
    requestBody?: Match
  ): CancelablePromise<Match> {
    return __request(OpenAPI, {
      method: "PATCH",
      url: "/meta/tournaments/{tournament_pk}/matches/{id}/",
      path: {
        tournament_pk: tournamentPk,
        id: id,
      },
      body: requestBody,
      mediaType: "application/json",
    });
  }

  /**
   * @param tournamentPk
   * @param id A unique integer value identifying this match.
   * @returns void
   * @throws ApiError
   */
  public static destroyMatch(
    tournamentPk: string,
    id: string
  ): CancelablePromise<void> {
    return __request(OpenAPI, {
      method: "DELETE",
      url: "/meta/tournaments/{tournament_pk}/matches/{id}/",
      path: {
        tournament_pk: tournamentPk,
        id: id,
      },
    });
  }

  /**
   * @param tournamentPk
   * @returns MatchTeam
   * @throws ApiError
   */
  public static listMatchTeams(
    tournamentPk: string
  ): CancelablePromise<Array<MatchTeam>> {
    return __request(OpenAPI, {
      method: "GET",
      url: "/meta/tournaments/{tournament_pk}/matchteams/",
      path: {
        tournament_pk: tournamentPk,
      },
    });
  }

  /**
   * @param tournamentPk
   * @param requestBody
   * @returns MatchTeam
   * @throws ApiError
   */
  public static createMatchTeam(
    tournamentPk: string,
    requestBody?: MatchTeam
  ): CancelablePromise<MatchTeam> {
    return __request(OpenAPI, {
      method: "POST",
      url: "/meta/tournaments/{tournament_pk}/matchteams/",
      path: {
        tournament_pk: tournamentPk,
      },
      body: requestBody,
      mediaType: "application/json",
    });
  }

  /**
   * @param tournamentPk
   * @param id A unique integer value identifying this match team.
   * @returns MatchTeam
   * @throws ApiError
   */
  public static retrieveMatchTeam(
    tournamentPk: string,
    id: string
  ): CancelablePromise<MatchTeam> {
    return __request(OpenAPI, {
      method: "GET",
      url: "/meta/tournaments/{tournament_pk}/matchteams/{id}/",
      path: {
        tournament_pk: tournamentPk,
        id: id,
      },
    });
  }

  /**
   * @param tournamentPk
   * @param id A unique integer value identifying this match team.
   * @param requestBody
   * @returns MatchTeam
   * @throws ApiError
   */
  public static updateMatchTeam(
    tournamentPk: string,
    id: string,
    requestBody?: MatchTeam
  ): CancelablePromise<MatchTeam> {
    return __request(OpenAPI, {
      method: "PUT",
      url: "/meta/tournaments/{tournament_pk}/matchteams/{id}/",
      path: {
        tournament_pk: tournamentPk,
        id: id,
      },
      body: requestBody,
      mediaType: "application/json",
    });
  }

  /**
   * @param tournamentPk
   * @param id A unique integer value identifying this match team.
   * @param requestBody
   * @returns MatchTeam
   * @throws ApiError
   */
  public static partialUpdateMatchTeam(
    tournamentPk: string,
    id: string,
    requestBody?: MatchTeam
  ): CancelablePromise<MatchTeam> {
    return __request(OpenAPI, {
      method: "PATCH",
      url: "/meta/tournaments/{tournament_pk}/matchteams/{id}/",
      path: {
        tournament_pk: tournamentPk,
        id: id,
      },
      body: requestBody,
      mediaType: "application/json",
    });
  }

  /**
   * @param tournamentPk
   * @param id A unique integer value identifying this match team.
   * @returns void
   * @throws ApiError
   */
  public static destroyMatchTeam(
    tournamentPk: string,
    id: string
  ): CancelablePromise<void> {
    return __request(OpenAPI, {
      method: "DELETE",
      url: "/meta/tournaments/{tournament_pk}/matchteams/{id}/",
      path: {
        tournament_pk: tournamentPk,
        id: id,
      },
    });
  }

  /**
   * @param tournamentPk
   * @param id A unique integer value identifying this match team.
   * @returns MatchTeam
   * @throws ApiError
   */
  public static canUpdateMatchTeam(
    tournamentPk: string,
    id: string
  ): CancelablePromise<boolean> {
    return __request(OpenAPI, {
      method: "GET",
      url: "/meta/tournaments/{tournament_pk}/matchteams/{id}/can_update/",
      path: {
        tournament_pk: tournamentPk,
        id: id,
      },
    });
  }

  /**
   * @param tournamentPk
   * @returns Order
   * @throws ApiError
   */
  public static listOrders(
    tournamentPk: string
  ): CancelablePromise<Array<Order>> {
    return __request(OpenAPI, {
      method: "GET",
      url: "/meta/tournaments/{tournament_pk}/orders/",
      path: {
        tournament_pk: tournamentPk,
      },
    });
  }

  /**
   * @param tournamentPk
   * @param requestBody
   * @returns Order
   * @throws ApiError
   */
  public static createOrder(
    tournamentPk: string,
    requestBody?: Order
  ): CancelablePromise<Order> {
    return __request(OpenAPI, {
      method: "POST",
      url: "/meta/tournaments/{tournament_pk}/orders/",
      path: {
        tournament_pk: tournamentPk,
      },
      body: requestBody,
      mediaType: "application/json",
    });
  }

  /**
   * @param tournamentPk
   * @param id A unique integer value identifying this order.
   * @returns Order
   * @throws ApiError
   */
  public static retrieveOrder(
    tournamentPk: string,
    id: string
  ): CancelablePromise<Order> {
    return __request(OpenAPI, {
      method: "GET",
      url: "/meta/tournaments/{tournament_pk}/orders/{id}/",
      path: {
        tournament_pk: tournamentPk,
        id: id,
      },
    });
  }

  /**
   * @param tournamentPk
   * @param id A unique integer value identifying this order.
   * @param requestBody
   * @returns Order
   * @throws ApiError
   */
  public static updateOrder(
    tournamentPk: string,
    id: string,
    requestBody?: Order
  ): CancelablePromise<Order> {
    return __request(OpenAPI, {
      method: "PUT",
      url: "/meta/tournaments/{tournament_pk}/orders/{id}/",
      path: {
        tournament_pk: tournamentPk,
        id: id,
      },
      body: requestBody,
      mediaType: "application/json",
    });
  }

  /**
   * @param tournamentPk
   * @param id A unique integer value identifying this order.
   * @param requestBody
   * @returns Order
   * @throws ApiError
   */
  public static partialUpdateOrder(
    tournamentPk: string,
    id: string,
    requestBody?: Order
  ): CancelablePromise<Order> {
    return __request(OpenAPI, {
      method: "PATCH",
      url: "/meta/tournaments/{tournament_pk}/orders/{id}/",
      path: {
        tournament_pk: tournamentPk,
        id: id,
      },
      body: requestBody,
      mediaType: "application/json",
    });
  }

  /**
   * @param tournamentPk
   * @param id A unique integer value identifying this order.
   * @returns void
   * @throws ApiError
   */
  public static destroyOrder(
    tournamentPk: string,
    id: string
  ): CancelablePromise<void> {
    return __request(OpenAPI, {
      method: "DELETE",
      url: "/meta/tournaments/{tournament_pk}/orders/{id}/",
      path: {
        tournament_pk: tournamentPk,
        id: id,
      },
    });
  }

  /**
   * @param tournamentPk
   * @returns Order
   * @throws ApiError
   */
  public static statsOrder(
    tournamentPk: string
  ): CancelablePromise<Array<OrderStats>> {
    return __request(OpenAPI, {
      method: "GET",
      url: "/meta/tournaments/{tournament_pk}/orders/stats/",
      path: {
        tournament_pk: tournamentPk,
      },
    });
  }

  /**
   * @param tournamentPk
   * @returns TournamentPhase
   * @throws ApiError
   */
  public static listTournamentPhases(
    tournamentPk: string
  ): CancelablePromise<Array<TournamentPhase>> {
    return __request(OpenAPI, {
      method: "GET",
      url: "/meta/tournaments/{tournament_pk}/phases/",
      path: {
        tournament_pk: tournamentPk,
      },
    });
  }

  /**
   * @param tournamentPk
   * @param requestBody
   * @returns TournamentPhase
   * @throws ApiError
   */
  public static createTournamentPhase(
    tournamentPk: string,
    requestBody?: TournamentPhase
  ): CancelablePromise<TournamentPhase> {
    return __request(OpenAPI, {
      method: "POST",
      url: "/meta/tournaments/{tournament_pk}/phases/",
      path: {
        tournament_pk: tournamentPk,
      },
      body: requestBody,
      mediaType: "application/json",
    });
  }

  /**
   * @param tournamentPk
   * @param id A unique integer value identifying this tournament phase.
   * @returns TournamentPhase
   * @throws ApiError
   */
  public static retrieveTournamentPhase(
    tournamentPk: string,
    id: string
  ): CancelablePromise<TournamentPhase> {
    return __request(OpenAPI, {
      method: "GET",
      url: "/meta/tournaments/{tournament_pk}/phases/{id}/",
      path: {
        tournament_pk: tournamentPk,
        id: id,
      },
    });
  }

  /**
   * @param tournamentPk
   * @param id A unique integer value identifying this tournament phase.
   * @param requestBody
   * @returns TournamentPhase
   * @throws ApiError
   */
  public static updateTournamentPhase(
    tournamentPk: string,
    id: string,
    requestBody?: TournamentPhase
  ): CancelablePromise<TournamentPhase> {
    return __request(OpenAPI, {
      method: "PUT",
      url: "/meta/tournaments/{tournament_pk}/phases/{id}/",
      path: {
        tournament_pk: tournamentPk,
        id: id,
      },
      body: requestBody,
      mediaType: "application/json",
    });
  }

  /**
   * @param tournamentPk
   * @param id A unique integer value identifying this tournament phase.
   * @param requestBody
   * @returns TournamentPhase
   * @throws ApiError
   */
  public static partialUpdateTournamentPhase(
    tournamentPk: string,
    id: string,
    requestBody?: TournamentPhase
  ): CancelablePromise<TournamentPhase> {
    return __request(OpenAPI, {
      method: "PATCH",
      url: "/meta/tournaments/{tournament_pk}/phases/{id}/",
      path: {
        tournament_pk: tournamentPk,
        id: id,
      },
      body: requestBody,
      mediaType: "application/json",
    });
  }

  /**
   * @param tournamentPk
   * @param id A unique integer value identifying this tournament phase.
   * @returns void
   * @throws ApiError
   */
  public static destroyTournamentPhase(
    tournamentPk: string,
    id: string
  ): CancelablePromise<void> {
    return __request(OpenAPI, {
      method: "DELETE",
      url: "/meta/tournaments/{tournament_pk}/phases/{id}/",
      path: {
        tournament_pk: tournamentPk,
        id: id,
      },
    });
  }

  /**
   * @param tournamentPk
   * @returns PhaseSeed
   * @throws ApiError
   */
  public static listPhaseSeeds(
    tournamentPk: string
  ): CancelablePromise<Array<PhaseSeed>> {
    return __request(OpenAPI, {
      method: "GET",
      url: "/meta/tournaments/{tournament_pk}/phaseseeds/",
      path: {
        tournament_pk: tournamentPk,
      },
    });
  }

  /**
   * @param tournamentPk
   * @param requestBody
   * @returns PhaseSeed
   * @throws ApiError
   */
  public static createPhaseSeed(
    tournamentPk: string,
    requestBody?: PhaseSeed
  ): CancelablePromise<PhaseSeed> {
    return __request(OpenAPI, {
      method: "POST",
      url: "/meta/tournaments/{tournament_pk}/phaseseeds/",
      path: {
        tournament_pk: tournamentPk,
      },
      body: requestBody,
      mediaType: "application/json",
    });
  }

  /**
   * @param tournamentPk
   * @param id A unique integer value identifying this phase seed.
   * @returns PhaseSeed
   * @throws ApiError
   */
  public static retrievePhaseSeed(
    tournamentPk: string,
    id: string
  ): CancelablePromise<PhaseSeed> {
    return __request(OpenAPI, {
      method: "GET",
      url: "/meta/tournaments/{tournament_pk}/phaseseeds/{id}/",
      path: {
        tournament_pk: tournamentPk,
        id: id,
      },
    });
  }

  /**
   * @param tournamentPk
   * @param id A unique integer value identifying this phase seed.
   * @param requestBody
   * @returns PhaseSeed
   * @throws ApiError
   */
  public static updatePhaseSeed(
    tournamentPk: string,
    id: string,
    requestBody?: PhaseSeed
  ): CancelablePromise<PhaseSeed> {
    return __request(OpenAPI, {
      method: "PUT",
      url: "/meta/tournaments/{tournament_pk}/phaseseeds/{id}/",
      path: {
        tournament_pk: tournamentPk,
        id: id,
      },
      body: requestBody,
      mediaType: "application/json",
    });
  }

  /**
   * @param tournamentPk
   * @param id A unique integer value identifying this phase seed.
   * @param requestBody
   * @returns PhaseSeed
   * @throws ApiError
   */
  public static partialUpdatePhaseSeed(
    tournamentPk: string,
    id: string,
    requestBody?: PhaseSeed
  ): CancelablePromise<PhaseSeed> {
    return __request(OpenAPI, {
      method: "PATCH",
      url: "/meta/tournaments/{tournament_pk}/phaseseeds/{id}/",
      path: {
        tournament_pk: tournamentPk,
        id: id,
      },
      body: requestBody,
      mediaType: "application/json",
    });
  }

  /**
   * @param tournamentPk
   * @param id A unique integer value identifying this phase seed.
   * @returns void
   * @throws ApiError
   */
  public static destroyPhaseSeed(
    tournamentPk: string,
    id: string
  ): CancelablePromise<void> {
    return __request(OpenAPI, {
      method: "DELETE",
      url: "/meta/tournaments/{tournament_pk}/phaseseeds/{id}/",
      path: {
        tournament_pk: tournamentPk,
        id: id,
      },
    });
  }

  /**
   * @param tournamentPk
   * @returns TournamentProduct
   * @throws ApiError
   */
  public static listTournamentProducts(
    tournamentPk: string
  ): CancelablePromise<Array<WithId<TournamentProduct>>> {
    return __request(OpenAPI, {
      method: "GET",
      url: "/meta/tournaments/{tournament_pk}/products/",
      path: {
        tournament_pk: tournamentPk,
      },
    });
  }

  /**
   * @param tournamentPk
   * @param requestBody
   * @returns TournamentProduct
   * @throws ApiError
   */
  public static createTournamentProduct(
    tournamentPk: string,
    requestBody?: TournamentProduct
  ): CancelablePromise<TournamentProduct> {
    return __request(OpenAPI, {
      method: "POST",
      url: "/meta/tournaments/{tournament_pk}/products/",
      path: {
        tournament_pk: tournamentPk,
      },
      body: requestBody,
      mediaType: "application/json",
    });
  }

  /**
   * @param tournamentPk
   * @param id A unique integer value identifying this tournament product.
   * @returns TournamentProduct
   * @throws ApiError
   */
  public static retrieveTournamentProduct(
    tournamentPk: string,
    id: string
  ): CancelablePromise<TournamentProduct> {
    return __request(OpenAPI, {
      method: "GET",
      url: "/meta/tournaments/{tournament_pk}/products/{id}/",
      path: {
        tournament_pk: tournamentPk,
        id: id,
      },
    });
  }

  /**
   * @param tournamentPk
   * @param id A unique integer value identifying this tournament product.
   * @param requestBody
   * @returns TournamentProduct
   * @throws ApiError
   */
  public static updateTournamentProduct(
    tournamentPk: string,
    id: string,
    requestBody?: TournamentProduct
  ): CancelablePromise<TournamentProduct> {
    return __request(OpenAPI, {
      method: "PUT",
      url: "/meta/tournaments/{tournament_pk}/products/{id}/",
      path: {
        tournament_pk: tournamentPk,
        id: id,
      },
      body: requestBody,
      mediaType: "application/json",
    });
  }

  /**
   * @param tournamentPk
   * @param id A unique integer value identifying this tournament product.
   * @param requestBody
   * @returns TournamentProduct
   * @throws ApiError
   */
  public static partialUpdateTournamentProduct(
    tournamentPk: string,
    id: string,
    requestBody?: TournamentProduct
  ): CancelablePromise<TournamentProduct> {
    return __request(OpenAPI, {
      method: "PATCH",
      url: "/meta/tournaments/{tournament_pk}/products/{id}/",
      path: {
        tournament_pk: tournamentPk,
        id: id,
      },
      body: requestBody,
      mediaType: "application/json",
    });
  }

  /**
   * @param tournamentPk
   * @param id A unique integer value identifying this tournament product.
   * @returns void
   * @throws ApiError
   */
  public static destroyTournamentProduct(
    tournamentPk: string,
    id: string
  ): CancelablePromise<void> {
    return __request(OpenAPI, {
      method: "DELETE",
      url: "/meta/tournaments/{tournament_pk}/products/{id}/",
      path: {
        tournament_pk: tournamentPk,
        id: id,
      },
    });
  }

  /**
   * @param tournamentPk
   * @returns TournamentTeam
   * @throws ApiError
   */
  public static listTournamentTeams(
    tournamentPk: string
  ): CancelablePromise<Array<WithId<TournamentTeam>>> {
    return __request(OpenAPI, {
      method: "GET",
      url: "/meta/tournaments/{tournament_pk}/teams/",
      path: {
        tournament_pk: tournamentPk,
      },
    });
  }

  /**
   * @param tournamentPk
   * @param requestBody
   * @returns TournamentTeam
   * @throws ApiError
   */
  public static createTournamentTeam(
    tournamentPk: string,
    requestBody?: TournamentTeam
  ): CancelablePromise<TournamentTeam> {
    return __request(OpenAPI, {
      method: "POST",
      url: "/meta/tournaments/{tournament_pk}/teams/",
      path: {
        tournament_pk: tournamentPk,
      },
      body: requestBody,
      mediaType: "application/json",
    });
  }

  /**
   * @param tournamentPk
   * @param id A unique integer value identifying this tournament team.
   * @returns TournamentTeam
   * @throws ApiError
   */
  public static retrieveTournamentTeam(
    tournamentPk: string,
    id: string
  ): CancelablePromise<TournamentTeam> {
    return __request(OpenAPI, {
      method: "GET",
      url: "/meta/tournaments/{tournament_pk}/teams/{id}/",
      path: {
        tournament_pk: tournamentPk,
        id: id,
      },
    });
  }

  /**
   * @param tournamentPk
   * @param id A unique integer value identifying this tournament team.
   * @param requestBody
   * @returns TournamentTeam
   * @throws ApiError
   */
  public static updateTournamentTeam(
    tournamentPk: string,
    id: string,
    requestBody?: TournamentTeam
  ): CancelablePromise<TournamentTeam> {
    return __request(OpenAPI, {
      method: "PUT",
      url: "/meta/tournaments/{tournament_pk}/teams/{id}/",
      path: {
        tournament_pk: tournamentPk,
        id: id,
      },
      body: requestBody,
      mediaType: "application/json",
    });
  }

  /**
   * @param tournamentPk
   * @param id A unique integer value identifying this tournament team.
   * @param requestBody
   * @returns TournamentTeam
   * @throws ApiError
   */
  public static partialUpdateTournamentTeam(
    tournamentPk: string,
    id: string,
    requestBody?: TournamentTeam
  ): CancelablePromise<TournamentTeam> {
    return __request(OpenAPI, {
      method: "PATCH",
      url: "/meta/tournaments/{tournament_pk}/teams/{id}/",
      path: {
        tournament_pk: tournamentPk,
        id: id,
      },
      body: requestBody,
      mediaType: "application/json",
    });
  }

  /**
   * @param tournamentPk
   * @param id A unique integer value identifying this tournament team.
   * @returns void
   * @throws ApiError
   */
  public static destroyTournamentTeam(
    tournamentPk: string,
    id: string
  ): CancelablePromise<void> {
    return __request(OpenAPI, {
      method: "DELETE",
      url: "/meta/tournaments/{tournament_pk}/teams/{id}/",
      path: {
        tournament_pk: tournamentPk,
        id: id,
      },
    });
  }

  /**
   * @param tournamentPk
   * @returns TournamentTeamPlayer
   * @throws ApiError
   */
  public static listTournamentTeamPlayers(
    tournamentPk: string
  ): CancelablePromise<Array<TournamentTeamPlayer>> {
    return __request(OpenAPI, {
      method: "GET",
      url: "/meta/tournaments/{tournament_pk}/tournamentteamplayers/",
      path: {
        tournament_pk: tournamentPk,
      },
    });
  }

  /**
   * @param tournamentPk
   * @param requestBody
   * @returns TournamentTeamPlayer
   * @throws ApiError
   */
  public static createTournamentTeamPlayer(
    tournamentPk: string,
    requestBody?: TournamentTeamPlayer
  ): CancelablePromise<TournamentTeamPlayer> {
    return __request(OpenAPI, {
      method: "POST",
      url: "/meta/tournaments/{tournament_pk}/tournamentteamplayers/",
      path: {
        tournament_pk: tournamentPk,
      },
      body: requestBody,
      mediaType: "application/json",
    });
  }

  /**
   * @param tournamentPk
   * @param id A unique integer value identifying this tournament team player.
   * @returns TournamentTeamPlayer
   * @throws ApiError
   */
  public static retrieveTournamentTeamPlayer(
    tournamentPk: string,
    id: string
  ): CancelablePromise<TournamentTeamPlayer> {
    return __request(OpenAPI, {
      method: "GET",
      url: "/meta/tournaments/{tournament_pk}/tournamentteamplayers/{id}/",
      path: {
        tournament_pk: tournamentPk,
        id: id,
      },
    });
  }

  /**
   * @param tournamentPk
   * @param id A unique integer value identifying this tournament team player.
   * @param requestBody
   * @returns TournamentTeamPlayer
   * @throws ApiError
   */
  public static updateTournamentTeamPlayer(
    tournamentPk: string,
    id: string,
    requestBody?: TournamentTeamPlayer
  ): CancelablePromise<TournamentTeamPlayer> {
    return __request(OpenAPI, {
      method: "PUT",
      url: "/meta/tournaments/{tournament_pk}/tournamentteamplayers/{id}/",
      path: {
        tournament_pk: tournamentPk,
        id: id,
      },
      body: requestBody,
      mediaType: "application/json",
    });
  }

  /**
   * @param tournamentPk
   * @param id A unique integer value identifying this tournament team player.
   * @param requestBody
   * @returns TournamentTeamPlayer
   * @throws ApiError
   */
  public static partialUpdateTournamentTeamPlayer(
    tournamentPk: string,
    id: string,
    requestBody?: TournamentTeamPlayer
  ): CancelablePromise<TournamentTeamPlayer> {
    return __request(OpenAPI, {
      method: "PATCH",
      url: "/meta/tournaments/{tournament_pk}/tournamentteamplayers/{id}/",
      path: {
        tournament_pk: tournamentPk,
        id: id,
      },
      body: requestBody,
      mediaType: "application/json",
    });
  }

  /**
   * @param tournamentPk
   * @param id A unique integer value identifying this tournament team player.
   * @returns void
   * @throws ApiError
   */
  public static destroyTournamentTeamPlayer(
    tournamentPk: string,
    id: string
  ): CancelablePromise<void> {
    return __request(OpenAPI, {
      method: "DELETE",
      url: "/meta/tournaments/{tournament_pk}/tournamentteamplayers/{id}/",
      path: {
        tournament_pk: tournamentPk,
        id: id,
      },
    });
  }

  /**
   * @param personPk
   * @returns PersonClub
   * @throws ApiError
   */
  public static listPersonClubs(
    personPk: string
  ): CancelablePromise<Array<PersonClub>> {
    return __request(OpenAPI, {
      method: "GET",
      url: "/meta/persons/{person_pk}/clubs/",
      path: {
        person_pk: personPk,
      },
    });
  }

  /**
   * @param personPk
   * @param requestBody
   * @returns PersonClub
   * @throws ApiError
   */
  public static createPersonClub(
    personPk: string,
    requestBody?: PersonClub
  ): CancelablePromise<PersonClub> {
    return __request(OpenAPI, {
      method: "POST",
      url: "/meta/persons/{person_pk}/clubs/",
      path: {
        person_pk: personPk,
      },
      body: requestBody,
      mediaType: "application/json",
    });
  }

  /**
   * @param personPk
   * @param id A unique integer value identifying this person club.
   * @returns PersonClub
   * @throws ApiError
   */
  public static retrievePersonClub(
    personPk: string,
    id: string
  ): CancelablePromise<PersonClub> {
    return __request(OpenAPI, {
      method: "GET",
      url: "/meta/persons/{person_pk}/clubs/{id}/",
      path: {
        person_pk: personPk,
        id: id,
      },
    });
  }

  /**
   * @param personPk
   * @param id A unique integer value identifying this person club.
   * @param requestBody
   * @returns PersonClub
   * @throws ApiError
   */
  public static updatePersonClub(
    personPk: string,
    id: string,
    requestBody?: PersonClub
  ): CancelablePromise<PersonClub> {
    return __request(OpenAPI, {
      method: "PUT",
      url: "/meta/persons/{person_pk}/clubs/{id}/",
      path: {
        person_pk: personPk,
        id: id,
      },
      body: requestBody,
      mediaType: "application/json",
    });
  }

  /**
   * @param personPk
   * @param id A unique integer value identifying this person club.
   * @param requestBody
   * @returns PersonClub
   * @throws ApiError
   */
  public static partialUpdatePersonClub(
    personPk: string,
    id: string,
    requestBody?: PersonClub
  ): CancelablePromise<PersonClub> {
    return __request(OpenAPI, {
      method: "PATCH",
      url: "/meta/persons/{person_pk}/clubs/{id}/",
      path: {
        person_pk: personPk,
        id: id,
      },
      body: requestBody,
      mediaType: "application/json",
    });
  }

  /**
   * @param personPk
   * @param id A unique integer value identifying this person club.
   * @returns void
   * @throws ApiError
   */
  public static destroyPersonClub(
    personPk: string,
    id: string
  ): CancelablePromise<void> {
    return __request(OpenAPI, {
      method: "DELETE",
      url: "/meta/persons/{person_pk}/clubs/{id}/",
      path: {
        person_pk: personPk,
        id: id,
      },
    });
  }

  /**
   * @param tournamentPk
   * @param orderPk
   * @returns OrderItem
   * @throws ApiError
   */
  public static listOrderItems(
    tournamentPk: string,
    orderPk: string
  ): CancelablePromise<Array<OrderItem>> {
    return __request(OpenAPI, {
      method: "GET",
      url: "/meta/tournaments/{tournament_pk}/orders/{order_pk}/order_items/",
      path: {
        tournament_pk: tournamentPk,
        order_pk: orderPk,
      },
    });
  }

  /**
   * @param tournamentPk
   * @param orderPk
   * @param requestBody
   * @returns OrderItem
   * @throws ApiError
   */
  public static createOrderItem(
    tournamentPk: string,
    orderPk: string,
    requestBody?: OrderItem
  ): CancelablePromise<OrderItem> {
    return __request(OpenAPI, {
      method: "POST",
      url: "/meta/tournaments/{tournament_pk}/orders/{order_pk}/order_items/",
      path: {
        tournament_pk: tournamentPk,
        order_pk: orderPk,
      },
      body: requestBody,
      mediaType: "application/json",
    });
  }

  /**
   * @param tournamentPk
   * @param orderPk
   * @param id A unique integer value identifying this order item.
   * @returns OrderItem
   * @throws ApiError
   */
  public static retrieveOrderItem(
    tournamentPk: string,
    orderPk: string,
    id: string
  ): CancelablePromise<OrderItem> {
    return __request(OpenAPI, {
      method: "GET",
      url: "/meta/tournaments/{tournament_pk}/orders/{order_pk}/order_items/{id}/",
      path: {
        tournament_pk: tournamentPk,
        order_pk: orderPk,
        id: id,
      },
    });
  }

  /**
   * @param tournamentPk
   * @param orderPk
   * @param id A unique integer value identifying this order item.
   * @param requestBody
   * @returns OrderItem
   * @throws ApiError
   */
  public static updateOrderItem(
    tournamentPk: string,
    orderPk: string,
    id: string,
    requestBody?: OrderItem
  ): CancelablePromise<OrderItem> {
    return __request(OpenAPI, {
      method: "PUT",
      url: "/meta/tournaments/{tournament_pk}/orders/{order_pk}/order_items/{id}/",
      path: {
        tournament_pk: tournamentPk,
        order_pk: orderPk,
        id: id,
      },
      body: requestBody,
      mediaType: "application/json",
    });
  }

  /**
   * @param tournamentPk
   * @param orderPk
   * @param id A unique integer value identifying this order item.
   * @param requestBody
   * @returns OrderItem
   * @throws ApiError
   */
  public static partialUpdateOrderItem(
    tournamentPk: string,
    orderPk: string,
    id: string,
    requestBody?: OrderItem
  ): CancelablePromise<OrderItem> {
    return __request(OpenAPI, {
      method: "PATCH",
      url: "/meta/tournaments/{tournament_pk}/orders/{order_pk}/order_items/{id}/",
      path: {
        tournament_pk: tournamentPk,
        order_pk: orderPk,
        id: id,
      },
      body: requestBody,
      mediaType: "application/json",
    });
  }

  /**
   * @param tournamentPk
   * @param orderPk
   * @param id A unique integer value identifying this order item.
   * @returns void
   * @throws ApiError
   */
  public static destroyOrderItem(
    tournamentPk: string,
    orderPk: string,
    id: string
  ): CancelablePromise<void> {
    return __request(OpenAPI, {
      method: "DELETE",
      url: "/meta/tournaments/{tournament_pk}/orders/{order_pk}/order_items/{id}/",
      path: {
        tournament_pk: tournamentPk,
        order_pk: orderPk,
        id: id,
      },
    });
  }
}
