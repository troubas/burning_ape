/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { CancelablePromise } from "../core/CancelablePromise";
import { OpenAPI } from "../core/OpenAPI";
import { request as __request } from "../core/request";

export class AuthService {
  /**
   * @returns any
   * @throws ApiError
   */
  public static listisLoggedIns(): CancelablePromise<boolean> {
    return __request(OpenAPI, {
      method: "GET",
      url: "/auth/is_logged_in/",
    });
  }
}
