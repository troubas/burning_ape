/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type PersonClub = {
  readonly id?: number;
  readonly url?: string;
  club: string;
  person: string;
  readonly date_from?: string;
  date_until?: string | null;
  player_number: string;
};
