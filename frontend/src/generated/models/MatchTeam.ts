/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type MatchTeam = {
  readonly id?: number;
  attitude_and_self_control?: MatchTeam.attitude_and_self_control | null;
  communication?: MatchTeam.communication | null;
  fair_mindedness?: MatchTeam.fair_mindedness | null;
  fouls_and_body_contact?: MatchTeam.fouls_and_body_contact | null;
  placeholder?: string;
  rules_knowledge_and_use?: MatchTeam.rules_knowledge_and_use | null;
  score?: number | null;
  display?: string;
  tournament_team_id?: number;
  spirit_remarks?: string | null;
};

export namespace MatchTeam {
  export enum attitude_and_self_control {
    "_4" = 4,
    "_3" = 3,
    "_2" = 2,
    "_1" = 1,
    "_0" = 0,
  }

  export enum communication {
    "_4" = 4,
    "_3" = 3,
    "_2" = 2,
    "_1" = 1,
    "_0" = 0,
  }

  export enum fair_mindedness {
    "_4" = 4,
    "_3" = 3,
    "_2" = 2,
    "_1" = 1,
    "_0" = 0,
  }

  export enum fouls_and_body_contact {
    "_4" = 4,
    "_3" = 3,
    "_2" = 2,
    "_1" = 1,
    "_0" = 0,
  }

  export enum rules_knowledge_and_use {
    "_4" = 4,
    "_3" = 3,
    "_2" = 2,
    "_1" = 1,
    "_0" = 0,
  }
}
