/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type TournamentProduct = {
  readonly id?: number;
  readonly tournament_id?: string;
  readonly category?: string;
  readonly product_name?: string;
  readonly product_id?: number;
  price?: number;
};
