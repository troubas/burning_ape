/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type ZipCode = {
  readonly id?: number;
  readonly url?: string;
  country: string;
  readonly country_name?: string;
  zip_code: string;
  locality: string;
};
