/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type Address = {
  readonly id?: number;
  readonly url?: string;
  line1?: string;
  line2?: string;
  line3?: string;
  line4?: string;
  zip_code: string;
};
