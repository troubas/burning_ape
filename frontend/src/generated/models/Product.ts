/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type Product = {
  readonly id?: number;
  readonly url?: string;
  category?: Product.category | null;
  product_name: string;
  price?: number;
};

export namespace Product {
  export enum category {
    FOOD = "Food",
    DRINKS = "Drinks",
    MERCH = "Merch",
  }
}
