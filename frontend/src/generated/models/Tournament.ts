/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type Tournament = {
  readonly id?: number;
  readonly url?: string;
  date: string;
  game_mode?: Tournament.game_mode | null;
  location: {
    readonly id?: number;
    readonly url?: string;
    line1?: string;
    line2?: string;
    line3?: string;
    line4?: string;
    zip_code: string;
  };
  name: string;
  readonly teams?: Array<string>;
};

export namespace Tournament {
  export enum game_mode {
    MEN = "Men",
    MIXED = "Mixed",
    OPEN = "Open",
    WOMAN = "Woman",
  }
}
