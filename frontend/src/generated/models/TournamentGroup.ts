/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type TournamentGroup = {
  readonly id?: number;
  readonly phase?: string | null;
  name: string;
  short_name: string;
};
