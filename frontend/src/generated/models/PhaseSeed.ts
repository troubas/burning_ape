/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type PhaseSeed = {
  readonly id?: number;
  readonly group?: string | null;
  readonly phase?: string | null;
  readonly team?: string | null;
  rank: number;
};
