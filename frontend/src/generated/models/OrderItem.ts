/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type OrderItem = {
  readonly id?: number;
  readonly order_id?: string;
  product_id?: number;
  readonly product_name?: string;
  quantity: number;
  price: number;
};
