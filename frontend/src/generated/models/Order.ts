/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type Order = {
  readonly id?: number;
  person_id: number;
  person_name?: string;
  order_items: Array<{
    readonly id?: number;
    readonly order_id?: string;
    product_id?: number;
    readonly product_name?: string;
    quantity: number;
    price: number;
  }>;
  readonly total_price?: number;
};
