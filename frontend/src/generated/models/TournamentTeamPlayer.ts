/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type TournamentTeamPlayer = {
  player_id?: number;
  player: {
    readonly id?: number;
    first_name: string;
    last_name?: string;
  };
  tournament_team_id: number;
  readonly public_name?: string;
};
