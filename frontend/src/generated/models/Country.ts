/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type Country = {
  readonly id?: number;
  readonly url?: string;
  country: string;
};
