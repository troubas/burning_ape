/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type Field = {
  readonly id?: number;
  readonly url?: string;
  location: string;
  name: string;
  length?: number | null;
  width?: number | null;
};
