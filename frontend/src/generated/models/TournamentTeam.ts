/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type TournamentTeam = {
  readonly id?: number;
  contact: number;
  readonly tournament?: string;
  registration_date?: string;
  team_name: string;
  readonly tournament_team_players?: Array<{
    player_id?: number;
    player: {
      readonly id?: number;
      first_name: string;
      last_name?: string;
    };
    tournament_team_id: number;
    readonly public_name?: string;
  }>;
};
