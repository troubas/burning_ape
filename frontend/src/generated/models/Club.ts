/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type Club = {
  readonly id?: number;
  name: string;
  provenance: number | null;
  contact_mechanism: Array<number>;
  readonly players?: Array<string>;
};
