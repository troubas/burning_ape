/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import { MatchTeam } from "./MatchTeam";

export type Match = {
  readonly id?: number;
  left_team: MatchTeam;
  right_team: MatchTeam;
  tournament: string;
  readonly tournament_id?: string;
  allow_draw?: boolean;
  name: string;
  played?: boolean;
  readonly match_start?: string;
  readonly match_end?: string;
  readonly field_name?: string;
};

export namespace Match {
  export enum attitude_and_self_control {
    "_4" = 4,
    "_3" = 3,
    "_2" = 2,
    "_1" = 1,
    "_0" = 0,
  }

  export enum communication {
    "_4" = 4,
    "_3" = 3,
    "_2" = 2,
    "_1" = 1,
    "_0" = 0,
  }

  export enum fair_mindedness {
    "_4" = 4,
    "_3" = 3,
    "_2" = 2,
    "_1" = 1,
    "_0" = 0,
  }

  export enum fouls_and_body_contact {
    "_4" = 4,
    "_3" = 3,
    "_2" = 2,
    "_1" = 1,
    "_0" = 0,
  }

  export enum rules_knowledge_and_use {
    "_4" = 4,
    "_3" = 3,
    "_2" = 2,
    "_1" = 1,
    "_0" = 0,
  }
}
