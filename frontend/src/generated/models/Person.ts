/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type Person = {
  readonly id?: number;
  readonly url?: string;
  readonly age?: string;
  birthday_validated?: boolean;
  first_name: string;
  last_name?: string;
  readonly full_name?: string;
  readonly teams?: Array<string>;
};
