/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type TournamentPhase = {
  readonly id?: number;
  readonly url?: string | null;
  tournament: string;
  game_mode: TournamentPhase.game_mode;
  name: string;
  phase_number: number;
  size: number;
  groups: Array<{
    readonly id?: number;
    readonly phase?: string | null;
    name: string;
    short_name: string;
  }>;
};

export namespace TournamentPhase {
  export enum game_mode {
    SWISS_DRAW = "Swiss Draw",
    ROUND_ROBIN = "Round Robin",
    KNOCKOUT = "Knockout",
  }
}
