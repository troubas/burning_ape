/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
export const $Product = {
  properties: {
    id: {
      type: "number",
      isReadOnly: true,
    },
    url: {
      type: "string",
      isReadOnly: true,
    },
    category: {
      type: "Enum",
      isNullable: true,
    },
    product_name: {
      type: "string",
      isRequired: true,
      maxLength: 64,
    },
    price: {
      type: "number",
    },
  },
} as const;
