/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
export const $TournamentGroup = {
  properties: {
    id: {
      type: "number",
      isReadOnly: true,
    },
    phase: {
      type: "string",
      isReadOnly: true,
      isNullable: true,
    },
    name: {
      type: "string",
      isRequired: true,
      maxLength: 128,
    },
    short_name: {
      type: "string",
      isRequired: true,
      maxLength: 4,
    },
  },
} as const;
