/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
export const $ZipCode = {
  properties: {
    id: {
      type: "number",
      isReadOnly: true,
    },
    url: {
      type: "string",
      isReadOnly: true,
    },
    country: {
      type: "string",
      isRequired: true,
    },
    country_name: {
      type: "string",
      isReadOnly: true,
    },
    zip_code: {
      type: "string",
      isRequired: true,
      maxLength: 32,
    },
    locality: {
      type: "string",
      isRequired: true,
      maxLength: 256,
    },
  },
} as const;
