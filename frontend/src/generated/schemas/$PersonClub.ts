/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
export const $PersonClub = {
  properties: {
    id: {
      type: "number",
      isReadOnly: true,
    },
    url: {
      type: "string",
      isReadOnly: true,
    },
    club: {
      type: "string",
      isRequired: true,
    },
    person: {
      type: "string",
      isRequired: true,
    },
    date_from: {
      type: "string",
      isReadOnly: true,
      format: "date",
    },
    date_until: {
      type: "string",
      isNullable: true,
      format: "date",
    },
    player_number: {
      type: "string",
      isRequired: true,
      maxLength: 8,
    },
  },
} as const;
