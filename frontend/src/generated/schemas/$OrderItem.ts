/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
export const $OrderItem = {
  properties: {
    id: {
      type: "number",
      isReadOnly: true,
    },
    order_id: {
      type: "string",
      isReadOnly: true,
    },
    product_id: {
      type: "number",
      isRequired: true,
    },
    product_name: {
      type: "string",
      isReadOnly: true,
    },
    quantity: {
      type: "number",
      isRequired: true,
      maximum: 2147483647,
      minimum: -2147483648,
    },
    price: {
      type: "number",
      isRequired: true,
    },
  },
} as const;
