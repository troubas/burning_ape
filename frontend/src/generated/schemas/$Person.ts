/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
export const $Person = {
  properties: {
    id: {
      type: "number",
      isReadOnly: true,
    },
    url: {
      type: "string",
      isReadOnly: true,
    },
    age: {
      type: "string",
      isReadOnly: true,
    },
    birthday_validated: {
      type: "boolean",
    },
    first_name: {
      type: "string",
      isRequired: true,
      maxLength: 128,
    },
    last_name: {
      type: "string",
      maxLength: 128,
    },
    full_name: {
      type: "string",
      isReadOnly: true,
    },
    teams: {
      type: "array",
      contains: {
        type: "string",
      },
      isReadOnly: true,
    },
  },
} as const;
