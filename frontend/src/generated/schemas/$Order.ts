/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
export const $Order = {
  properties: {
    id: {
      type: "number",
      isReadOnly: true,
    },
    person_id: {
      type: "number",
      isRequired: true,
    },
    person_name: {
      type: "string",
    },
    order_items: {
      type: "array",
      contains: {
        properties: {
          id: {
            type: "number",
            isReadOnly: true,
          },
          order_id: {
            type: "string",
            isReadOnly: true,
          },
          product_id: {
            type: "number",
            isRequired: true,
          },
          product_name: {
            type: "string",
            isReadOnly: true,
          },
          quantity: {
            type: "number",
            isRequired: true,
            maximum: 2147483647,
            minimum: -2147483648,
          },
          price: {
            type: "number",
            isRequired: true,
          },
        },
      },
      isRequired: true,
    },
    total_price: {
      type: "number",
      isReadOnly: true,
    },
  },
} as const;
