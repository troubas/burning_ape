/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
export const $Country = {
  properties: {
    id: {
      type: "number",
      isReadOnly: true,
    },
    url: {
      type: "string",
      isReadOnly: true,
    },
    country: {
      type: "string",
      isRequired: true,
      maxLength: 128,
    },
  },
} as const;
