/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
export const $Address = {
  properties: {
    id: {
      type: "number",
      isReadOnly: true,
    },
    url: {
      type: "string",
      isReadOnly: true,
    },
    line1: {
      type: "string",
      maxLength: 256,
    },
    line2: {
      type: "string",
      maxLength: 256,
    },
    line3: {
      type: "string",
      maxLength: 256,
    },
    line4: {
      type: "string",
      maxLength: 256,
    },
    zip_code: {
      type: "string",
      isRequired: true,
    },
  },
} as const;
