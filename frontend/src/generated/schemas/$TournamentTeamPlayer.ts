/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
export const $TournamentTeamPlayer = {
  properties: {
    player_id: {
      type: "number",
    },
    player: {
      properties: {
        id: {
          type: "number",
          isReadOnly: true,
        },
        first_name: {
          type: "string",
          isRequired: true,
          maxLength: 128,
        },
        last_name: {
          type: "string",
          maxLength: 128,
        },
      },
      isRequired: true,
    },
    tournament_team_id: {
      type: "number",
      isRequired: true,
    },
    public_name: {
      type: "string",
      isReadOnly: true,
    },
  },
} as const;
