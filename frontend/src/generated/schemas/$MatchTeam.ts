/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
export const $MatchTeam = {
  properties: {
    id: {
      type: "number",
      isReadOnly: true,
    },
    attitude_and_self_control: {
      type: "Enum",
      isNullable: true,
    },
    communication: {
      type: "Enum",
      isNullable: true,
    },
    fair_mindedness: {
      type: "Enum",
      isNullable: true,
    },
    fouls_and_body_contact: {
      type: "Enum",
      isNullable: true,
    },
    placeholder: {
      type: "string",
      maxLength: 128,
    },
    rules_knowledge_and_use: {
      type: "Enum",
      isNullable: true,
    },
    score: {
      type: "number",
      isNullable: true,
      maximum: 2147483647,
      minimum: -2147483648,
    },
    display: {
      type: "string",
      isReadOnly: true,
    },
    spirit_remarks: {
      type: "string",
      isNullable: true,
      maxLength: 2048,
    },
  },
} as const;
