/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
export const $Match = {
  properties: {
    id: {
      type: "number",
      isReadOnly: true,
    },
    left_team: {
      properties: {
        id: {
          type: "number",
          isReadOnly: true,
        },
        attitude_and_self_control: {
          type: "Enum",
          isNullable: true,
        },
        communication: {
          type: "Enum",
          isNullable: true,
        },
        fair_mindedness: {
          type: "Enum",
          isNullable: true,
        },
        fouls_and_body_contact: {
          type: "Enum",
          isNullable: true,
        },
        placeholder: {
          type: "string",
          maxLength: 128,
        },
        rules_knowledge_and_use: {
          type: "Enum",
          isNullable: true,
        },
        score: {
          type: "number",
          isNullable: true,
          maximum: 2147483647,
          minimum: -2147483648,
        },
        display: {
          type: "string",
          isReadOnly: true,
        },
        spirit_remarks: {
          type: "string",
          isNullable: true,
          maxLength: 2048,
        },
      },
      isRequired: true,
    },
    right_team: {
      properties: {
        id: {
          type: "number",
          isReadOnly: true,
        },
        attitude_and_self_control: {
          type: "Enum",
          isNullable: true,
        },
        communication: {
          type: "Enum",
          isNullable: true,
        },
        fair_mindedness: {
          type: "Enum",
          isNullable: true,
        },
        fouls_and_body_contact: {
          type: "Enum",
          isNullable: true,
        },
        placeholder: {
          type: "string",
          maxLength: 128,
        },
        rules_knowledge_and_use: {
          type: "Enum",
          isNullable: true,
        },
        score: {
          type: "number",
          isNullable: true,
          maximum: 2147483647,
          minimum: -2147483648,
        },
        display: {
          type: "string",
          isReadOnly: true,
        },
        spirit_remarks: {
          type: "string",
          isNullable: true,
          maxLength: 2048,
        },
      },
      isRequired: true,
    },
    tournament: {
      type: "string",
      isRequired: true,
    },
    tournament_id: {
      type: "string",
      isReadOnly: true,
    },
    allow_draw: {
      type: "boolean",
    },
    name: {
      type: "string",
      isRequired: true,
      maxLength: 128,
    },
    played: {
      type: "boolean",
    },
    match_start: {
      type: "string",
      isReadOnly: true,
      format: "date-time",
    },
    match_end: {
      type: "string",
      isReadOnly: true,
      format: "date-time",
    },
    field_name: {
      type: "string",
      isReadOnly: true,
    },
  },
} as const;
