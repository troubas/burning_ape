/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
export const $TournamentPhase = {
  properties: {
    id: {
      type: "number",
      isReadOnly: true,
    },
    url: {
      type: "string",
      isReadOnly: true,
      isNullable: true,
    },
    tournament: {
      type: "string",
      isRequired: true,
    },
    game_mode: {
      type: "Enum",
      isRequired: true,
    },
    name: {
      type: "string",
      isRequired: true,
      maxLength: 128,
    },
    phase_number: {
      type: "number",
      isRequired: true,
      maximum: 32767,
    },
    size: {
      type: "number",
      isRequired: true,
      maximum: 32767,
    },
    groups: {
      type: "array",
      contains: {
        properties: {
          id: {
            type: "number",
            isReadOnly: true,
          },
          phase: {
            type: "string",
            isReadOnly: true,
            isNullable: true,
          },
          name: {
            type: "string",
            isRequired: true,
            maxLength: 128,
          },
          short_name: {
            type: "string",
            isRequired: true,
            maxLength: 4,
          },
        },
      },
      isRequired: true,
    },
  },
} as const;
