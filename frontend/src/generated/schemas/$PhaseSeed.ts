/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
export const $PhaseSeed = {
  properties: {
    id: {
      type: "number",
      isReadOnly: true,
    },
    group: {
      type: "string",
      isReadOnly: true,
      isNullable: true,
    },
    phase: {
      type: "string",
      isReadOnly: true,
      isNullable: true,
    },
    team: {
      type: "string",
      isReadOnly: true,
      isNullable: true,
    },
    rank: {
      type: "number",
      isRequired: true,
      maximum: 2147483647,
      minimum: -2147483648,
    },
  },
} as const;
