/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
export const $User = {
  properties: {
    id: {
      type: "number",
      isReadOnly: true,
    },
    url: {
      type: "string",
      isReadOnly: true,
    },
    username: {
      type: "string",
      description: `Required. 150 characters or fewer. Letters, digits and @/./+/-/_ only.`,
      isRequired: true,
      maxLength: 150,
      pattern: "^[\\w.@+-]+\\z",
    },
    first_name: {
      type: "string",
      maxLength: 150,
    },
    last_name: {
      type: "string",
      maxLength: 150,
    },
    email: {
      type: "string",
      format: "email",
      maxLength: 254,
    },
  },
} as const;
