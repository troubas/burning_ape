/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
export const $Field = {
  properties: {
    id: {
      type: "number",
      isReadOnly: true,
    },
    url: {
      type: "string",
      isReadOnly: true,
    },
    location: {
      type: "string",
      isRequired: true,
    },
    name: {
      type: "string",
      isRequired: true,
      maxLength: 128,
    },
    length: {
      type: "number",
      isNullable: true,
      maximum: 2147483647,
      minimum: -2147483648,
    },
    width: {
      type: "number",
      isNullable: true,
      maximum: 2147483647,
      minimum: -2147483648,
    },
  },
} as const;
