/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
export const $Club = {
  properties: {
    id: {
      type: "number",
      isReadOnly: true,
    },
    name: {
      type: "string",
      isRequired: true,
      maxLength: 128,
    },
    provenance: {
      type: "number",
      isRequired: true,
      isNullable: true,
    },
    contact_mechanism: {
      type: "array",
      contains: {
        type: "number",
      },
      isRequired: true,
    },
    players: {
      type: "array",
      contains: {
        type: "string",
      },
      isReadOnly: true,
    },
  },
} as const;
