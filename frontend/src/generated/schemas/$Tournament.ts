/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
export const $Tournament = {
  properties: {
    id: {
      type: "number",
      isReadOnly: true,
    },
    url: {
      type: "string",
      isReadOnly: true,
    },
    date: {
      type: "string",
      isRequired: true,
      format: "date",
    },
    game_mode: {
      type: "Enum",
      isNullable: true,
    },
    location: {
      properties: {
        id: {
          type: "number",
          isReadOnly: true,
        },
        url: {
          type: "string",
          isReadOnly: true,
        },
        line1: {
          type: "string",
          maxLength: 256,
        },
        line2: {
          type: "string",
          maxLength: 256,
        },
        line3: {
          type: "string",
          maxLength: 256,
        },
        line4: {
          type: "string",
          maxLength: 256,
        },
        zip_code: {
          type: "string",
          isRequired: true,
        },
      },
      isRequired: true,
    },
    name: {
      type: "string",
      isRequired: true,
      maxLength: 128,
    },
    teams: {
      type: "array",
      contains: {
        type: "string",
      },
      isReadOnly: true,
    },
  },
} as const;
