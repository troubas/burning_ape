/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
export const $TournamentProduct = {
  properties: {
    id: {
      type: "number",
      isReadOnly: true,
    },
    tournament_id: {
      type: "string",
      isReadOnly: true,
    },
    category: {
      type: "string",
      isReadOnly: true,
    },
    product_name: {
      type: "string",
      isReadOnly: true,
    },
    price: {
      type: "number",
    },
  },
} as const;
