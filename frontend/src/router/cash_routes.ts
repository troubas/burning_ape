import AddPlayerViewVue from "@/views/cash/AddPlayerView.vue";
import CashRegisterViewVue from "@/views/cash/CashRegisterView.vue";
import LottoViewVue from "@/views/cash/LottoView.vue";
import OrderViewVue from "@/views/cash/OrderView.vue";
import TournamentViewVue from "@/views/cash/TournamentView.vue";
import { RouteRecordRaw } from "vue-router";

const cashRoutes: RouteRecordRaw[] = [
  {
    path: "/tournaments",
    name: "tournament",
    component: TournamentViewVue,
  },
  {
    path: "/tournaments/:tournament_id/orders",
    name: "order",
    component: OrderViewVue,
  },
  {
    path: "/tournaments/:tournament_id/cash_register",
    name: "cash_register",
    component: CashRegisterViewVue,
    props: true,
  },
  {
    path: "/tournaments/:tournament_id/add_player",
    name: "add_player",
    component: AddPlayerViewVue,
    props: true,
  },
  {
    path: "/lotto",
    name: "lotto",
    component: LottoViewVue,
    props: true,
  },
];

export default cashRoutes;
