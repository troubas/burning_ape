import { RouteRecordRaw } from "vue-router";
import TeamsView from "../views/matches/TeamsView.vue";
import MatchesView from "../views/matches/MatchesView.vue";
import MatchDetailView from "../views/matches/MatchDetailView.vue";
import TeamDetailView from "../views/matches/TeamDetailView.vue";
import MatchTeamFormView from "../views/matches/MatchTeamFormView.vue";
import ChallengesView from "../views/matches/ChallengesView.vue";
import StoryView from "../views/matches/StoryView.vue";
import TimeTableView from "../views/matches/TimeTableView.vue";

const matchRoutes: RouteRecordRaw[] = [
  {
    path: "/m/:tournament_id/teams",
    name: "tournament_teams",
    component: TeamsView,
    meta: {
      layout: "DefaultLayout",
    },
  },
  {
    path: "/m/:tournament_id/teams/:team_id",
    name: "tournament_team_detail",
    component: TeamDetailView,
    meta: {
      layout: "DefaultLayout",
    },
  },
  {
    path: "/m/:tournament_id/matches",
    name: "tournament_matches",
    component: MatchesView,
    meta: {
      layout: "DefaultLayout",
    },
  },
  {
    path: "/m/:tournament_id/matches/:match_id",
    name: "tournament_match_detail",
    component: MatchDetailView,
    meta: {
      layout: "DefaultLayout",
    },
  },
  {
    path: "/m/:tournament_id/match_teams/:match_team_id",
    name: "tournament_match_team_form",
    component: MatchTeamFormView,
    meta: {
      layout: "DefaultLayout",
    },
  },
  {
    path: "/m/:tournament_id/challenges",
    name: "tournament_challenges",
    component: ChallengesView,
    meta: {
      layout: "DefaultLayout",
    },
  },
  {
    path: "/m/:tournament_id/story",
    name: "tournament_story",
    component: StoryView,
    meta: {
      layout: "DefaultLayout",
    },
  },
  {
    path: "/m/:tournament_id/timetable",
    name: "tournament_time_table",
    component: TimeTableView,
    meta: {
      layout: "DefaultLayout",
    },
  },
];

export default matchRoutes;
