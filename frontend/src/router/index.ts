import { createRouter, createWebHistory, RouteRecordRaw } from "vue-router";
import IndexView from "../views/IndexView.vue";
import PageNotFoundView from "../views/PageNotFoundView.vue";
import cashRoutes from "./cash_routes";
import matchRoutes from "./match_routes";

const baseRoutes: RouteRecordRaw[] = [
  {
    path: "/",
    name: "index",
    component: IndexView,
  },
  { path: "/:pathMatch(.*)*", component: PageNotFoundView },
];

const routes = baseRoutes
  .concat(cashRoutes as RouteRecordRaw[])
  .concat(matchRoutes as RouteRecordRaw[]);

const router = createRouter({
  history: createWebHistory(
    process.env.NODE_ENV === "development" ? "" : "/app/"
  ),
  routes,
});

export default router;
