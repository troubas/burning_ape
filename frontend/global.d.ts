// Define Global types here

export type WithId<T> = T & { id: number };
export type Option<T> = T | null | undefined;
