// Define Global types here

type WithId<T> = T & { id: number };
type Option<T> = T | null | undefined;
