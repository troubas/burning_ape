import { generate } from "openapi-typescript-codegen";

generate({
  input: "../openapi-schema.yml",
  output: "./src/generated",
  exportCore: false,
  exportServices: false,
  exportModels: true,
  exportSchemas: true,
  httpClient: "axios",
  indent: "2",
});
