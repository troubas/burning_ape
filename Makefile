# Try to automate some things.

.DEFAULT_GOAL := help


# You can set these variables from the command line, and also
# from the environment for the first two.
SPHINXOPTS    ?=
SPHINXBUILD   ?= $(VENV)/sphinx-build
SOURCEDIR     = docs/source
BUILDDIR      = docs/build

# Texinfo
MAKEINFO = makeinfo --no-split

# linux only. Haters will hate.
GREEN  := $(shell tput -Txterm setaf 2)
YELLOW := $(shell tput -Txterm setaf 3)
WHITE  := $(shell tput -Txterm setaf 7)
RESET  := $(shell tput -Txterm sgr0)

TARGET_MAX_CHAR_NUM=20
TOURNAMENT?="Banana Cup 3.0"


all: help


## Generate an ERD for the burning_ape_meta module
model_burning_ape_meta.pdf: model_burning_ape_meta.dot
	dot -Tpdf $< > $@

model_burning_ape_meta.dot: venv
	$(VENV)/python manage.py graph_models burning_ape_meta -X "Historical*, Tournament" -o $@

.PHONY: help
## Show this help text
help:
	@echo ''
	@echo 'Usage:'
	@echo '  ${YELLOW}make${RESET} ${GREEN}<target>${RESET}'
	@echo ''
	@echo ''
	@echo 'Targets:'
	@printf "  ${YELLOW}%-$(TARGET_MAX_CHAR_NUM)s${RESET} ${GREEN}%s${RESET}\n" 'bash' 'Set-Up Venv and launch interactive bourne shell';
	@printf "  ${YELLOW}%-$(TARGET_MAX_CHAR_NUM)s${RESET} ${GREEN}%s${RESET}\n" 'zsh' 'Set-Up Venv and launch interactive zsh shell';
	@awk '/^[a-zA-Z\-_0-9]+:/ { \
		helpMessage = match(lastLine, /^## (.*)/); \
		if (helpMessage) { \
			helpCommand = substr($$1, 0, index($$1, ":")-1); \
			helpMessage = substr(lastLine, RSTART + 3, RLENGTH); \
			printf "  ${YELLOW}%-$(TARGET_MAX_CHAR_NUM)s${RESET} ${GREEN}%s${RESET}\n", helpCommand, helpMessage; \
		} \
	} \
	{ lastLine = $$0 }' $(MAKEFILE_LIST)
	@echo ''


.PHONY: test
## Run Tests
test: venv
	$(VENV)/python manage.py test

.PHONY: devserver
## Run development server (detached from console session)
devserver: venv
	tmux start-server
	# start new session
	tmux new-session -d -s "burning-ape" -n server
	tmux splitw -h
	# Select pane 1, run django webserver
	tmux selectp -t 0
	tmux send-keys "$(VENV)/python manage.py runserver" C-m
	# Select pane 1, set dir to frontend, run node dev environment
	tmux selectp -t 1
	tmux send-keys "cd frontend; yarn dev" C-m

	@echo '${GREEN}Successfully${RESET} created a new tmux session.'
	@echo ' * Use ${YELLOW}tmux attach${RESET} to connect.'
	@echo ' * Use ${YELLOW}make devserver-stop${RESET} to kill the session.'

## Stop development server
devserver-stop: venv
	tmux kill-session -t "burning-ape"

.PHONY: devshell
## enter python shell with loaded django environment
devshell: venv
	$(VENV)/python manage.py shell

## do migrations
migrations: venv
	$(VENV)/python manage.py makemigrations
	$(VENV)/python manage.py migrate

## init db
init_db: venv
	$(VENV)/python manage.py init_data

## create reports
reports: venv
	mkdir -p reports
	$(VENV)/python manage.py reports $(TOURNAMENT)

## deploy
deploy: venv
	cd frontend; yarn install; yarn run build
	python manage.py collectstatic
	sudo cp -R frontend/dist/* /srv/http/app
	sudo rm -rf /srv/http/app/static
	sudo mv static /srv/http/app/static
	sudo systemctl restart gunicorn.service

.PHONY: docs
## Build sphinx documentation (html)
docs: venv
	@$(SPHINXBUILD) -M html "$(SOURCEDIR)" "$(BUILDDIR)" $(SPHINXOPTS) $(O)

.PHONY: texinfo
texinfo: venv
	@$(SPHINXBUILD) -M texinfo "$(SOURCEDIR)" "$(BUILDDIR)" $(SPHINXOPTS) $(O)
	@$(MAKEINFO) -o docs/build/burningapeapp.info docs/build/texinfo/burningapeapp.texi

api_models: venv
	$(VENV)/python manage.py generateschema --file openapi-schema.yml
	cd frontend; npm run generate;
	cd frontend; yarn prettier --write src/generated/**/*
	cd frontend; yarn eslint --fix src/generated/**/*
	cd frontend; yarn prettier --write src/generated/index.ts
	cd frontend; yarn eslint --fix src/generated/index.ts

include Makefile.venv
