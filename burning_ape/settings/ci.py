from .base import *

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = "django-insecure-&@^^j3q0+mmu%8%4n!!qys=pr^7l_!71!vu=(05m6rhr)za_w#"

# SECURITY WARNING: define the correct hosts in production!
ALLOWED_HOSTS = ["*"]

EMAIL_BACKEND = "django.core.mail.backends.console.EmailBackend"

# A list of trusted origins for unsafe requests (e.g. POST).
# https://docs.djangoproject.com/en/4.2/ref/settings/#std-setting-CSRF_TRUSTED_ORIGINS
CSRF_TRUSTED_ORIGINS = ['https://localhost', 'http://localhost']
STATIC_ROOT = 'static'

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'burning_ape',
        'USER': 'burning_ape',
        'PASSWORD': 'burning_ape',
        'HOST' : 'postgres',
        'PORT' : '5432'

    }
}

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/4.0/howto/static-files/
STATIC_URL = 'static/'

try:
    from .local import *
except ImportError:
    pass
