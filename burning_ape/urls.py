
from django.contrib import admin
from django.urls import include, path

from rest_framework import (
    response,
)
from rest_framework.decorators import (
    api_view,
    permission_classes,
)


@api_view(['GET'])
@permission_classes([])
def is_logged_in(request, format=None):
    return response.Response(request.user.id != None)

urlpatterns = [
    path('admin/', admin.site.urls),
    path('auth/', include('rest_framework.urls', namespace='rest_framework')),
    path('meta/', include('burning_ape_meta.urls')),
    path('auth/is_logged_in/', is_logged_in),
]
